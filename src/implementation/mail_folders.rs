use crate::interface::{MailFoldersEmitter, MailFoldersTrait, MailFoldersTree};
use mailcore::Folder;
use regex::{escape, Regex};
use std::sync::{Arc, Mutex};

pub struct MailFoldersData {
    emit: MailFoldersEmitter,
    folders: Option<Vec<Folder>>,
}

impl MailFoldersData {
    pub fn set_folders(&mut self, folders: Vec<Folder>) {
        self.folders = Some(folders);
        self.emit.new_data_ready(None)
    }
}

/// Nodes in the tree
struct MailFolder {
    name: String,
    delimiter: String,
    unread: Option<usize>,
    /// position of the parent node in `folders` Vec
    parent: Option<usize>,
    /// position of the `folders` Vec
    subfolders: Vec<usize>,
}

/// Rust-side implementation of QAbstractItemModel via MailFoldersTrait
pub struct MailFolders {
    /// interface for emitting signals from any thread to the UI
    emit: MailFoldersEmitter,
    /// interface for emitting signals from the UI thread to the UI
    tree: MailFoldersTree,
    /// all nodes in the tree
    folders: Vec<MailFolder>,
    /// all nodes in the tree
    all_folders: Vec<Folder>,
    /// interface for sending new data to this object
    data: Arc<Mutex<MailFoldersData>>,
    filter: String,
}

impl MailFolders {
    /// Get a handle for setting new data on this object.
    /// This handle can be sent to a thread that aquires this information.
    pub fn data(&self) -> Arc<Mutex<MailFoldersData>> {
        self.data.clone()
    }
    fn add_folder(
        &mut self,
        parent: usize,
        name: String,
        delimiter: String,
        unread: Option<usize>,
    ) -> usize {
        let index = self.folders.len();
        self.folders.push(MailFolder {
            name,
            delimiter,
            parent: Some(parent),
            subfolders: Vec::new(),
            unread,
        });
        self.folders[parent].subfolders.push(index);
        index
    }
    fn init(&mut self) {
        self.folders.clear();
        // place the root node at index 0
        self.folders.push(MailFolder {
            name: "/".into(),
            delimiter: String::new(),
            unread: None,
            parent: None,
            subfolders: Vec::new(),
        });
    }
    pub fn set_folders(&mut self, folders: Vec<Folder>) {
        if self.all_folders == folders {
            return;
        }
        self.all_folders = folders;
        self.filter_folders();
    }
    fn filter_folders(&mut self) {
        let mut folders = self.all_folders.clone();
        let delimiter: &str = folders
            .first()
            .map(|n| n.name.delimiter.as_str())
            .unwrap_or("/");
        let regex = format!("{}[^{}]*$", escape(&self.filter), delimiter);
        let regex = Regex::new(&regex).unwrap();
        folders.retain(|f| regex.is_match(&f.name.name));
        // split the folder names at the delimiter and add them to the tree
        self.tree.begin_reset_model();
        self.init();
        for folder in folders {
            let mut index = 0;
            let mut full_name = String::new();
            for f in folder.name.name.split(&folder.name.delimiter) {
                let pos = if let Some(pos) = self.folders[index]
                    .subfolders
                    .iter()
                    .find(|i| self.folders[**i].name == f)
                {
                    Some(*pos)
                } else {
                    None
                };
                if !full_name.is_empty() {
                    full_name.push_str(&folder.name.delimiter);
                }
                full_name.push_str(f);
                let unread = self
                    .all_folders
                    .iter()
                    .find(|f| f.name.name == full_name)
                    .and_then(|f| f.unread);
                if let Some(pos) = pos {
                    index = pos;
                } else {
                    index = self.add_folder(index, f.into(), folder.name.delimiter.clone(), unread);
                }
            }
        }
        self.tree.end_reset_model();
    }
}

impl MailFoldersTrait for MailFolders {
    fn new(mut emit: MailFoldersEmitter, tree: MailFoldersTree) -> MailFolders {
        let mut i = MailFolders {
            emit: emit.clone(),
            tree,
            folders: Vec::new(),
            all_folders: Vec::new(),
            data: Arc::new(Mutex::new(MailFoldersData {
                emit,
                folders: None,
            })),
            filter: String::new(),
        };
        i.init();
        i
    }
    fn emit(&mut self) -> &mut MailFoldersEmitter {
        &mut self.emit
    }
    /// Returns the number of rows for a node with a given index.
    /// To get the number of root nodes, call `row_count(None)`.
    fn row_count(&self, index: Option<usize>) -> usize {
        self.folders[index.unwrap_or(0)].subfolders.len()
    }
    /// Returns the index for n-th row in parent.
    /// To get the index of the n-th root node, call `index(None, n)`.
    fn index(&self, parent: Option<usize>, row: usize) -> usize {
        self.folders[parent.unwrap_or(0)].subfolders[row]
    }
    /// Returns the index of the parent node or `None` if there is no parent.
    fn parent(&self, index: usize) -> Option<usize> {
        self.folders[index].parent
    }
    /// Returns the row number of a node with given index.
    fn row(&self, index: usize) -> usize {
        if let Some(parent) = self.folders[index].parent {
            self.folders[parent]
                .subfolders
                .iter()
                .position(|i| *i == index)
                .unwrap()
        } else {
            0
        }
    }
    /// Check if a node with given index and previous row number still exists.
    /// If it still exists return the new row number. Otherwise return `None`.
    /// This function is used to automatically update QPersistentModelIndexes when
    /// `layoutChanged()` is called.
    fn check_row(&self, index: usize, _row: usize) -> Option<usize> {
        if index < self.folders.len() {
            Some(self.row(index))
        } else {
            None
        }
    }
    fn name(&self, index: usize) -> &str {
        &self.folders[index].name
    }
    fn label(&self, index: usize) -> String {
        let unread = self.folders[index].unread.unwrap_or_default();
        let name = &self.folders[index].name;
        if unread > 0 {
            format!("{} ({})", name, unread)
        } else {
            name.to_string()
        }
    }
    fn delimiter(&self, index: usize) -> &str {
        &self.folders[index].delimiter
    }
    fn unread(&self, index: usize) -> u64 {
        self.folders[index].unread.unwrap_or_default() as u64
    }
    /// Returns true if more data is available to show in the UI.
    fn can_fetch_more(&self, _index: Option<usize>) -> bool {
        self.data.lock().unwrap().folders.is_some()
    }
    /// Show the available new data.
    fn fetch_more(&mut self, _index: Option<usize>) {
        let f = self.data.lock().unwrap().folders.take();
        if let Some(new_folders) = f {
            self.set_folders(new_folders);
        }
    }
    fn filter(&self) -> &str {
        &self.filter
    }
    fn set_filter(&mut self, filter: String) {
        self.filter = filter;
        self.filter_folders();
        self.emit.filter_changed()
    }
}
