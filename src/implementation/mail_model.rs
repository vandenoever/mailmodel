use super::folder_tree_listing::F;
use crate::implementation::{Email, EmailData, FolderTreeListing, MailFolders, MailFoldersData};
use crate::interface::{MailModelEmitter, MailModelTrait};
use mailcore::message::{Message, Uid};
use mailcore::{EmailReceiver, FoldersReceiver, Observer};
use mailcore::{Folder, Mail};
use std::path::PathBuf;
use std::sync::{Arc, Mutex};

pub struct MailModel {
    emit: MailModelEmitter,
    dialog_folders: MailFolders,
    folders: MailFolders,
    folder_threads: FolderTreeListing,
    current_folder: String,
    mail: Option<Mail>,
    config_file: Option<PathBuf>,
    email: Email,
}

#[derive(Clone)]
struct R(Arc<Mutex<MailFoldersData>>);

impl Observer for R {
    fn done(&self) -> bool {
        false
    }
}

impl FoldersReceiver for R {
    fn set_folders(&self, folders: Vec<Folder>) {
        self.0.lock().unwrap().set_folders(folders)
    }
}

#[derive(Clone)]
struct E(Arc<Mutex<EmailData>>);

impl EmailReceiver for E {
    fn set_email(&self, message: Option<Arc<Message>>) {
        self.0.lock().unwrap().set_email(message)
    }
}

impl MailModelTrait for MailModel {
    fn new(
        mut emit: MailModelEmitter,
        dialog_folders: MailFolders,
        email: Email,
        folder_threads: FolderTreeListing,
        folders: MailFolders,
    ) -> MailModel {
        MailModel {
            emit: emit.clone(),
            dialog_folders,
            folders,
            folder_threads,
            current_folder: "/".into(),
            mail: None,
            email,
            config_file: None,
        }
    }
    fn emit(&mut self) -> &mut MailModelEmitter {
        &mut self.emit
    }
    fn dialog_folders(&self) -> &MailFolders {
        &self.dialog_folders
    }
    fn dialog_folders_mut(&mut self) -> &mut MailFolders {
        &mut self.dialog_folders
    }
    fn folders(&self) -> &MailFolders {
        &self.folders
    }
    fn folders_mut(&mut self) -> &mut MailFolders {
        &mut self.folders
    }
    fn folder_threads(&self) -> &FolderTreeListing {
        &self.folder_threads
    }
    fn folder_threads_mut(&mut self) -> &mut FolderTreeListing {
        &mut self.folder_threads
    }
    fn current_folder(&self) -> &str {
        &self.current_folder
    }
    fn set_current_folder(&mut self, folder: String) {
        if self.current_folder == folder {
            return;
        }
        self.current_folder = folder;
        self.emit.current_folder_changed();
        if !self.current_folder.is_empty() {
            if let Some(mail) = &self.mail {
                if let Err(e) =
                    mail.observe_folder(&self.current_folder, F(self.folder_threads.data()))
                {
                    eprintln!("{:?}", e);
                }
            }
        }
    }
    fn email(&self) -> &Email {
        &self.email
    }
    fn email_mut(&mut self) -> &mut Email {
        &mut self.email
    }
    fn set_email(&mut self, uid: u64) {
        if let Some(mail) = &self.mail {
            if let Err(e) = mail.get_email(
                &self.current_folder,
                Uid(uid as usize),
                E(self.email.data()),
            ) {
                eprintln!("{:?}", e);
            }
        };
    }
    fn config_file(&self) -> Option<&str> {
        self.config_file.as_ref().and_then(|s| s.to_str())
    }
    fn set_config_file(&mut self, config_file: Option<String>) {
        self.config_file = config_file.map(|s| s.into());
        if let Some(config_file) = &self.config_file {
            let mail = match Mail::create(config_file.clone()) {
                Err(e) => {
                    eprintln!("{:?}", e);
                    return;
                }
                Ok(mail) => mail,
            };
            if let Err(e) = mail.observe_folders(R(self.folders.data())) {
                eprintln!("{:?}", e);
            }
            if let Err(e) = mail.observe_folders(R(self.dialog_folders.data())) {
                eprintln!("{:?}", e);
            }
            self.mail = Some(mail);
        }
    }
    fn send(&mut self, _bytes: &[u8]) {
        eprintln!("sending");
    }
    fn save_draft(&mut self, _bytes: &[u8]) {
        eprintln!("saving draft");
    }
}
