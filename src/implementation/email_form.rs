use crate::implementation::attachments::Attachments;
use crate::interface::{EmailFormEmitter, EmailFormList, EmailFormTrait};
use mailcore::new_message::*;

pub struct EmailForm {
    emit: EmailFormEmitter,
    attachments: Attachments,
    model: EmailFormList,
    new_message: NewMessage,
}

impl EmailFormTrait for EmailForm {
    fn new(emit: EmailFormEmitter, model: EmailFormList, attachments: Attachments) -> Self {
        let mut ef = EmailForm {
            emit,
            attachments,
            model,
            new_message: NewMessage::default(),
        };
        ef.new_message.recipients.push(Recipient {
            to_cc_or_bcc: "To".into(),
            address: "a@b.c".into(),
        });
        ef.new_message.recipients.push(Recipient {
            to_cc_or_bcc: "CC".into(),
            address: "j@i.p".into(),
        });
        ef
    }
    fn emit(&mut self) -> &mut EmailFormEmitter {
        &mut self.emit
    }
    fn subject(&self) -> &str {
        &self.new_message.subject
    }
    fn set_subject(&mut self, subject: String) {
        println!("{}", subject);
        self.new_message.subject = subject;
    }
    fn body(&self) -> &str {
        &self.new_message.body
    }
    fn set_body(&mut self, body: String) {
        self.new_message.body = body;
    }
    fn send_attachments(&self) -> &Attachments {
        &self.attachments
    }
    fn send_attachments_mut(&mut self) -> &mut Attachments {
        &mut self.attachments
    }
    fn row_count(&self) -> usize {
        self.new_message.recipients.len()
    }
    fn to_cc_or_bcc(&self, index: usize) -> String {
        let recipient = &self.new_message.recipients[index];
        let to_cc_or_bcc: &str = recipient.to_cc_or_bcc.into();
        to_cc_or_bcc.into()
    }
    fn set_to_cc_or_bcc(&mut self, index: usize, to_cc_or_bcc: String) -> bool {
        println!("{} {}", index, to_cc_or_bcc);
        self.new_message.recipients[index].to_cc_or_bcc = to_cc_or_bcc.as_str().into();
        true
    }
    fn address(&self, index: usize) -> &str {
        &self.new_message.recipients[index].address
    }
    fn set_address(&mut self, index: usize, address: String) -> bool {
        println!("{} {}", index, address);
        self.new_message.recipients[index].address = address;
        true
    }
    fn init_reply_all_from_mail_bytes(&mut self, bytes: &[u8]) {
        self.model.begin_reset_model();
        self.new_message = NewMessage::reply_all_from_mail_bytes(bytes);
        self.model.end_reset_model();
        self.emit.subject_changed();
        self.emit.body_changed();
    }
    fn init_reply_from_mail_bytes(&mut self, bytes: &[u8]) {
        self.model.begin_reset_model();
        self.new_message = NewMessage::reply_from_mail_bytes(bytes);
        self.model.end_reset_model();
        self.emit.subject_changed();
        self.emit.body_changed();
    }
    fn bytes<F>(&self, f: F)
    where
        F: FnOnce(&[u8]),
    {
        f(&self.new_message.bytes())
    }
}
