use crate::implementation::attachments::Attachments;
use crate::interface::{AttachmentsEmitter, AttachmentsTrait, EmailEmitter, EmailTrait};
use mailcore::message::Message;
use std::sync::{Arc, Mutex};

pub struct EmailData {
    emit: EmailEmitter,
    attachments_emit: AttachmentsEmitter,
    pub email: Option<Arc<Message>>,
}

impl EmailData {
    pub fn set_email(&mut self, message: Option<Arc<Message>>) {
        if self.email != message {
            self.email = message;
            self.emit.subject_changed();
            self.emit.from_changed();
            self.emit.date_changed();
            self.emit.body_changed();
            self.emit.html_changed();
            self.attachments_emit.new_data_ready();
        }
    }
}

pub struct Email {
    emit: EmailEmitter,
    data: Arc<Mutex<EmailData>>,
    attachments: Attachments,
}

impl Email {
    pub fn data(&self) -> Arc<Mutex<EmailData>> {
        self.data.clone()
    }
    fn get_alternative(&self, mimetype: &str) -> String {
        let data = self.data.lock().unwrap();
        if let Some(email) = &data.email {
            email.get_alternative(mimetype)
        } else {
            String::new()
        }
    }
}

impl EmailTrait for Email {
    fn new(mut emit: EmailEmitter, mut attachments: Attachments) -> Email {
        let data = Arc::new(Mutex::new(EmailData {
            emit: emit.clone(),
            email: None,
            attachments_emit: attachments.emit().clone(),
        }));
        attachments.set_email_data(Arc::clone(&data));
        Email {
            emit,
            data,
            attachments,
        }
    }
    fn emit(&mut self) -> &mut EmailEmitter {
        &mut self.emit
    }
    fn date<F>(&self, f: F)
    where
        F: FnOnce(&str),
    {
        if let Some(email) = &self.data.lock().unwrap().email {
            f(&email.date.to_rfc2822());
        } else {
            f("");
        }
    }
    fn from<F>(&self, f: F)
    where
        F: FnOnce(&str),
    {
        if let Some(email) = &self.data.lock().unwrap().email {
            f(&email.from);
        } else {
            f("");
        }
    }
    fn subject<F>(&self, f: F)
    where
        F: FnOnce(&str),
    {
        if let Some(email) = &self.data.lock().unwrap().email {
            f(&email.subject);
        } else {
            f("");
        }
    }
    fn body<F>(&self, f: F)
    where
        F: FnOnce(&str),
    {
        let plain = self.get_alternative("text/plain");
        f(&plain);
    }
    fn html<F>(&self, f: F)
    where
        F: FnOnce(&str),
    {
        let html = self.get_alternative("text/html");
        f(&html);
    }
    fn attachments(&self) -> &Attachments {
        &self.attachments
    }
    fn attachments_mut(&mut self) -> &mut Attachments {
        &mut self.attachments
    }
    fn bytes<F>(&self, f: F)
    where
        F: FnOnce(&[u8]),
    {
        let data = self.data.lock().unwrap();
        if let Some(email) = &data.email {
            f(&email.data)
        } else {
            f(b"")
        }
    }
}
