use crate::implementation::email::EmailData;
use crate::interface::{AttachmentsEmitter, AttachmentsList, AttachmentsTrait};
use mailcore::message::Message;
use mailparse::{parse_mail, ParsedMail};
use std::path::PathBuf;
use std::sync::{Arc, Mutex};

#[derive(Debug)]
pub struct Attachment {
    pub name: String,
    pub data: Vec<u8>,
}

pub struct Attachments {
    emit: AttachmentsEmitter,
    model: AttachmentsList,
    attachments: Vec<Attachment>,
    email_data: Option<Arc<Mutex<EmailData>>>,
    current_email: Option<Arc<Message>>,
}

impl Attachments {
    pub fn set_email_data(&mut self, email_data: Arc<Mutex<EmailData>>) {
        self.email_data = Some(email_data);
    }
    fn set_attachments(&mut self, attachments: Vec<Attachment>) {
        self.model.begin_reset_model();
        self.attachments = attachments;
        self.model.end_reset_model();
    }
}

impl AttachmentsTrait for Attachments {
    fn new(emit: AttachmentsEmitter, model: AttachmentsList) -> Attachments {
        Attachments {
            emit,
            model,
            attachments: Vec::new(),
            email_data: None,
            current_email: None,
        }
    }
    fn emit(&mut self) -> &mut AttachmentsEmitter {
        &mut self.emit
    }
    fn row_count(&self) -> usize {
        self.attachments.len()
    }
    fn name(&self, index: usize) -> &str {
        &self.attachments[index].name
    }
    fn bytes(&self, index: usize) -> &[u8] {
        &self.attachments[index].data
    }
    fn can_fetch_more(&self) -> bool {
        if let Some(email_data) = &self.email_data {
            if email_data.lock().unwrap().email != self.current_email {
                return true;
            }
        }
        false
    }
    fn fetch_more(&mut self) {
        if !self.can_fetch_more() {
            return;
        }
        let mut attachments = Vec::new();
        if let Some(email_data) = &self.email_data {
            if let Some(email) = &email_data.lock().unwrap().email {
                self.current_email = Some(Arc::clone(&email));
                let email = parse_mail(&email.data).unwrap();
                get_attachments(&mut attachments, &email);
            }
        }
        self.set_attachments(attachments);
    }
    // save attachment to a file
    // if there is an error, send a string of length > 1
    fn save_to_folder(&self, index: u32, folder: String) -> String {
        let mut path = PathBuf::new();
        if let Some(p) = folder.strip_prefix("file://") {
            path.push(p);
        } else {
            path.push(folder);
        }
        path.push(&self.attachments[index as usize].name);
        if path.exists() {
            return format!("File {} already exists.", path.to_string_lossy());
        }
        if let Err(e) = std::fs::write(path, &self.attachments[index as usize].data) {
            format!("{}", e)
        } else {
            String::new()
        }
    }
}

fn get_attachments(a: &mut Vec<Attachment>, part: &ParsedMail) {
    let name = part.ctype.params.get("name");
    if let (Some(name), Ok(data)) = (name, part.get_body_raw()) {
        a.push(Attachment {
            name: name.clone(),
            data,
        });
    }
    for part in &part.subparts {
        get_attachments(a, part);
    }
}
