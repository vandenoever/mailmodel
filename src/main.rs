mod implementation;

use std::os::raw::c_char;
extern "C" {
    fn main_cpp(app: *const c_char, config_file: *const c_char);
}

fn main() {
    use std::ffi::CString;
    let mut args = std::env::args();
    let app = CString::new(args.next().unwrap()).unwrap();
    let config_file = CString::new(
        args.next()
            .unwrap_or_else(|| panic!("Provide a file with mail configuration.")),
    )
    .unwrap();
    unsafe {
        main_cpp(app.as_ptr(), config_file.as_ptr());
    }
}

pub mod interface {
    include!(concat!(env!("OUT_DIR"), "/src/interface.rs"));
}
