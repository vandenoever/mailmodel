import QtQuick 2.7
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.2
import QtQml.Models 2.3

Dialog {
    id: dialog
    readonly property string currentFolder: tree.currentFolder
    title: qsTr('Select a folder')
    standardButtons: StandardButton.Ok | StandardButton.Cancel
    onAccepted: setFilter('')
    QtObject {
        id: p
        readonly property real backspace: 16777219
        readonly property real enter: 16777220

        function selectRecursive(index) {
            let name = tree.model.name(index);
            if (name.indexOf(tree.model.filter) !== -1) {
                tree.setCurrentIndex(index);
                return true;
            }
            var rowCount = tree.model.rowCount(index);
            var row;
            for (row = 0; row < rowCount; ++row) {
                if (selectRecursive(tree.model.index(row, 0, index))) {
                    return true;
                }
            }
            return false;
        }
    }
    ColumnLayout {
        anchors.fill: parent
        Text {
            id: text
            text: tree.model.filter
                ? tree.model.filter
                : qsTr("You can start typing to filter the list of folders.")
            Layout.fillWidth: true
        }
        Keys.onPressed: {
            if (event.key === p.backspace) {
                let len = tree.model.filter.length;
                if (len > 0) {
                    setFilter(tree.model.filter.substr(0, len - 1));
                }
            } else if (event.text) {
                setFilter(tree.model.filter + event.text);
                event.accepted = true;
            }
        }
        FolderTree {
            id: tree
            focus: true
            Layout.fillWidth: true
            Layout.fillHeight: true
            model: mailmodel.dialog_folders
            selectionMode: SelectionMode.SingleSelection
            selection: ItemSelectionModel {
                model: tree.model
            }
            Keys.onPressed: {
                if (event.key === p.enter) {
                    dialog.close();
                    dialog.accepted();
                    event.accepted = true;
                }
            }
        }
        Action {
            text: 'Clear filter'
            shortcut: StandardKey.Delete
            enabled: true
            onTriggered: setFilter('')
        }
        Action {
            text: 'Close dialog'
            shortcut: StandardKey.Cancel
            enabled: true
            onTriggered: dialog.close()
        }
    }
    onVisibleChanged: {
        tree.focus = true;
    }
    Component.onCompleted: {
        tree.model.modelReset.connect(function () {
            expandAll(tree);
        });
    }
    function setFilter(filter) {
        tree.model.filter = filter;
        if (filter) {
            p.selectRecursive(tree.rootIndex);
        }
    }
}
