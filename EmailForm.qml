import QtQuick 2.7
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Dialogs 1.2
import RustCode 1.0

ApplicationWindow {
    id: composer_window
    width: 640
    height: 1000
    signal closedWithSend(var bytes)
    signal closedWithSave(var bytes)
    EmailModel {
        id: email_model
    }
    QtObject {
        id: p
        property bool changed: false
    }
    toolBar: ToolBar {
        RowLayout {
            anchors.fill: parent
            ToolButton {
                action: sendAction
            }
            ToolButton {
                text: qsTr('Attach')
            }
            CheckBox {
                text: qsTr('Encrypt')
                checked: true
            }
            Item { Layout.fillWidth: true }
        }
    }
    ColumnLayout {
        anchors.fill: parent
        GridLayout {
            Layout.fillWidth: true
            columns: 2
            Repeater {
                model: email_model
                EmailAddresseeType {
                    Layout.row: index
                    Layout.column: 0
                    addresseeType: toCcOrBcc
//                    onAddresseeTypeChanged: toCcOrBcc = addresseeType
                }
            }
            Repeater {
                model: email_model
                TextField {
                    Layout.row: index
                    Layout.column: 1
                    Layout.fillWidth: true
                    text: address
                }
            }
            EmailAddresseeType {
                onAddresseeTypeChanged: p.changed = true
            }
            TextField {
                Layout.fillWidth: true
            }
            Label {
                text: qsTr("Subject:")
            }
            TextField {
                id: subject
                Layout.fillWidth: true
                text: email_model.subject
                onTextChanged: {
                    p.changed = true
                    email_model.subject = text
                }
            }
        }
        TextArea {
            id: body
            Layout.fillWidth: true
            Layout.fillHeight: true
            text: email_model.body
            onTextChanged: {
                p.changed = true
                email_model.body = text
            }
        }
        EmailSyntaxHighlighter {
            document: body.textDocument
        }
    }
    Component.onCompleted: p.changed = false
    Action {
        id: sendAction
        text: qsTr('Send')
        shortcut: "Ctrl+Return"
        onTriggered: {
            composer_window.closedWithSend(email_model.bytes)
            p.changed = false
            composer_window.close()
        }
    }
    onClosing: {
        console.log("CLOSING", p.changed)
        if (p.changed) {
            close.accepted = false
            close_dialog.visible = true
        }
    }
    Dialog {
        id: close_dialog
        title: qsTr("Close Composer")
        standardButtons: StandardButton.Save | StandardButton.Discard | StandardButton.Cancel
        Text {
            text: qsTr("Do you want to save the message for later or discard it?")
            textFormat: Text.PlainText
            wrapMode: Text.WordWrap
        }
        onAccepted: {
            composer_window.closedWithSave(email_model.bytes)
            closer.running = true
        }
        onDiscard: closer.running = true
        onRejected: close_dialog.visible = false
    }
    Timer {
        // this is required because calling composer_window.close() from a
        // dialog handler does not work
        id: closer
        interval: 1
        onTriggered: {
            p.changed = false
            composer_window.close()
        }
    }
    function initReplyFromMailBytes(bytes) {
        email_model.initReplyFromMailBytes(bytes)
        p.changed = false
    }
    function initReplyAllFromMailBytes(bytes) {
        email_model.initReplyAllFromMailBytes(bytes)
        p.changed = false
    }
}
