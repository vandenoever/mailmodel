use mailcore::{IndexedMailDirConfig, Mail, MailFlags, MailStoreConfig};
use std::fs;
use std::path::{Path, PathBuf};
use tempfile::TempDir;
use test_utilities::{EmailReceiver, FolderReceiver, FoldersReceiver};

#[test]
fn missing_config_file() {
    let r = Mail::create("missin.json");
    assert!(r.is_err());
}

fn clone_indexed_maildir<P: AsRef<Path>>(maildir_path: P) -> (TempDir, PathBuf) {
    let tmp_dir = TempDir::new().unwrap();
    let mut from = Vec::default();
    from.push(maildir_path);
    fs_extra::copy_items(&from, tmp_dir.path(), &fs_extra::dir::CopyOptions::new()).unwrap();
    let config = MailStoreConfig::IndexedMailDir(IndexedMailDirConfig {
        mail_dir: tmp_dir.path().join("mail"),
        index_file: tmp_dir.path().join("index.sqlite"),
    });
    let config_path = tmp_dir.path().join("config.json");
    fs::write(
        &config_path,
        serde_json::to_string_pretty(&config).unwrap().as_bytes(),
    )
    .unwrap();
    (tmp_dir, config_path)
}

#[test]
fn create_mail_core() {
    let (_tmp_dir, config_path) = clone_indexed_maildir("testdata/mail");
    Mail::create(config_path).unwrap();
}

#[test]
fn list_folders() {
    let (_tmp_dir, config_path) = clone_indexed_maildir("testdata/mail");
    let fr = FoldersReceiver::default();
    let mail = Mail::create(config_path).unwrap();
    mail.observe_folders(fr.clone()).unwrap();
    fr.wait_until(
        |d| d.updates == 3,
        |data| {
            let folders = &data.folders;
            assert_eq!(folders[0].name.name, "inbox");
        },
    );
}

#[test]
fn stop_updates_when_done() {
    let (_tmp_dir, config_path) = clone_indexed_maildir("testdata/mail");
    let mut fr1 = FoldersReceiver::default();
    fr1.set_done(true);
    let fr2 = FoldersReceiver::default();
    let mail = Mail::create(config_path).unwrap();
    mail.observe_folders(fr1.clone()).unwrap();
    mail.observe_folders(fr2.clone()).unwrap();
    // only fr2 should have the full list of folders
    fr2.wait_until(|d| d.updates == 2, |data| assert!(data.folders.len() == 1));
    fr1.wait_until(|d| d.updates == 1, |data| assert!(data.folders.is_empty()));
}

#[test]
fn list_inbox_mails() {
    let (_tmp_dir, config_path) = clone_indexed_maildir("testdata/mail");
    let fr = FolderReceiver::default();
    let mail = Mail::create(config_path).unwrap();
    mail.observe_folder("inbox", fr.clone()).unwrap();
    fr.wait_until(
        |d| d.updates == 2,
        |data| assert_eq!(data.tree.row_count(None), 1),
    );
}

#[test]
fn detect_mail_move() {
    // Move a mail from new to cur. This should change the boolean 'seen'
    // from false to true.
    let (tmp_dir, config_path) = clone_indexed_maildir("testdata/mail");
    let fr = FolderReceiver::default();
    let mail = Mail::create(config_path).unwrap();
    mail.observe_folder("inbox", fr.clone()).unwrap();
    fr.wait_until(
        |d| d.updates == 2,
        |data| {
            let tree = &data.tree;
            assert!(tree.row_count(None) == 1);
            let mail_index = tree.index(None, 0);
            let mail = &tree[mail_index];
            assert!(
                mail.flags
                    == Some(MailFlags {
                        seen: false,
                        ..Default::default()
                    })
            );
        },
    );
    fs::rename(
        tmp_dir.path().join("mail/inbox/new/1"),
        tmp_dir.path().join("mail/inbox/cur/1"),
    )
    .unwrap();
    fr.wait_until(
        |d| d.updates == 3,
        |data| {
            let tree = &data.tree;
            assert!(tree.row_count(None) == 1);
            let mail_index = tree.index(None, 0);
            let mail = &tree[mail_index];
            assert!(
                mail.flags
                    == Some(MailFlags {
                        seen: true,
                        ..Default::default()
                    })
            );
        },
    );
}

#[test]
fn detect_mail_move2() {
    // Move a mail from cur to new. This should change the boolean 'seen'
    // from true to false.
    let (tmp_dir, config_path) = clone_indexed_maildir("testdata/mail");
    fs::rename(
        tmp_dir.path().join("mail/inbox/new/1"),
        tmp_dir.path().join("mail/inbox/cur/1"),
    )
    .unwrap();
    let fr = FolderReceiver::default();
    let mail = Mail::create(config_path).unwrap();
    mail.observe_folder("inbox", fr.clone()).unwrap();
    fr.wait_until(
        |d| d.updates == 2,
        |d| {
            let tree = &d.tree;
            assert_eq!(tree.row_count(None), 1);
            assert!(tree.row_count(None) == 1);
            let mail_index = tree.index(None, 0);
            let mail = &tree[mail_index];
            assert!(
                mail.flags
                    == Some(MailFlags {
                        seen: true,
                        ..Default::default()
                    })
            );
        },
    );
    fs::rename(
        tmp_dir.path().join("mail/inbox/cur/1"),
        tmp_dir.path().join("mail/inbox/new/1"),
    )
    .unwrap();
    fr.wait_until(
        |d| d.updates == 3,
        |d| {
            let tree = &d.tree;
            assert_eq!(tree.row_count(None), 1);
            assert!(tree.row_count(None) == 1);
            let mail_index = tree.index(None, 0);
            let mail = &tree[mail_index];
            assert!(
                mail.flags
                    == Some(MailFlags {
                        seen: false,
                        ..Default::default()
                    })
            );
        },
    );
}

#[test]
fn detect_mail_folder_add() {
    let (_tmp_dir, config_path) = clone_indexed_maildir("testdata/mail");
    let fr = FoldersReceiver::default();
    let mail = Mail::create(config_path).unwrap();
    mail.observe_folders(fr.clone()).unwrap();
    fr.wait_until(|d| d.updates == 2, |d| assert_eq!(d.folders.len(), 1));
    fs::create_dir_all(_tmp_dir.path().join("mail/done/new")).unwrap();
    fs::create_dir_all(_tmp_dir.path().join("mail/done/cur")).unwrap();
    // it is not clear why the updates should be >= 4
    fr.wait_until(
        |d| d.updates == 4,
        |data| {
            let folders = &data.folders;
            assert_eq!(folders.len(), 2);
            assert!(folders.iter().any(|f| f.name.name == "done"));
            assert!(folders.iter().any(|f| f.name.name == "inbox"));
        },
    );
}

#[test]
fn get_message() {
    let (_tmp_dir, config_path) = clone_indexed_maildir("testdata/mail");
    let fr = FolderReceiver::default();
    let er = EmailReceiver::default();
    let mail = Mail::create(config_path).unwrap();
    mail.observe_folder("inbox", fr.clone()).unwrap();
    fr.wait_until(
        |d| d.updates == 2,
        |data| assert_eq!(data.tree.row_count(None), 1),
    );
    let data = fr.data();
    let tree = &data.tree;
    let msg_index = tree.index(None, 0);
    let msg = &tree[msg_index];
    mail.get_email("inbox", msg.uid, er.clone()).unwrap();
    er.wait_until(|d| d.updates == 1, |d| assert!(d.mail.is_some()));
}
