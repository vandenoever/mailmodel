#![allow(dead_code)]
use crate::tree::Tree;
use std::ops::Index;
use std::sync::Arc;

pub trait TreeListener {
    fn layout_about_to_be_changed(&mut self) {}
    fn layout_changed(&mut self) {}
    fn data_changed(&mut self, _first: usize, _last: usize) {}
    fn begin_reset_model(&mut self) {}
    fn end_reset_model(&mut self) {}
    fn begin_insert_rows(&mut self, _index: Option<usize>, _first: usize, _last: usize) {}
    fn end_insert_rows(&mut self) {}
    fn begin_move_rows(
        &mut self,
        _index: Option<usize>,
        _first: usize,
        _last: usize,
        _dest: Option<usize>,
        _destination: usize,
    ) {
    }
    fn end_move_rows(&mut self) {}
    fn begin_remove_rows(&mut self, _index: Option<usize>, _first: usize, _last: usize) {}
    fn end_remove_rows(&mut self) {}
}

#[derive(Clone)]
pub enum TreeChange<T> {
    Reset(Arc<Tree<T>>),
    Insert(Option<usize>, usize, T),
}

pub struct LoggingTree<T> {
    tree: Arc<Tree<T>>,
    log: Vec<TreeChange<T>>,
    continue_log: bool,
}

impl<T: Clone + PartialEq> LoggingTree<T> {
    pub fn new(tree: Arc<Tree<T>>, continue_log: bool) -> LoggingTree<T> {
        LoggingTree {
            continue_log: continue_log && tree.len() > 0,
            tree,
            log: Vec::new(),
        }
    }
    pub fn len(&self) -> usize {
        self.tree.len()
    }
    pub fn is_empty(&self) -> bool {
        self.tree.len() == 0
    }
    pub fn reset(&mut self, tree: Arc<Tree<T>>) {
        if !self.continue_log {
            self.tree = tree;
        } else if tree != self.tree {
            self.log.clear();
            self.continue_log = false;
            self.tree = tree;
        }
    }
    pub fn insert(&mut self, parent: Option<usize>, row: usize, t: T) -> usize {
        let index = Arc::make_mut(&mut self.tree).insert(parent, row, t.clone());
        if self.continue_log {
            self.log.push(TreeChange::Insert(parent, row, t));
        }
        index
    }
    pub fn prepend(&mut self, parent: Option<usize>, t: T) -> usize {
        self.insert(parent, 0, t)
    }
    pub fn append(&mut self, parent: Option<usize>, t: T) -> usize {
        let pos = self.tree.row_count(parent);
        self.insert(parent, pos, t)
    }
    pub fn parent(&self, index: usize) -> Option<usize> {
        self.tree.parent(index)
    }
    pub fn log(mut self) -> Vec<TreeChange<T>> {
        let mut log = Vec::new();
        if self.continue_log {
            std::mem::swap(&mut log, &mut self.log);
        } else {
            log.push(TreeChange::Reset(Arc::clone(&self.tree)));
        }
        log
    }
    pub fn tree(&self) -> &Arc<Tree<T>> {
        &self.tree
    }
    pub fn borrow_for_non_data_change(&mut self, index: usize) -> &mut T {
        &mut Arc::make_mut(&mut self.tree)[index]
    }
}

impl<T> Index<usize> for LoggingTree<T> {
    type Output = T;
    fn index(&self, index: usize) -> &T {
        &self.tree[index]
    }
}

impl<T, L: TreeListener> Index<usize> for SyncedTree<T, L> {
    type Output = T;
    fn index(&self, index: usize) -> &T {
        &self.tree[index]
    }
}

#[derive(Default)]
pub struct SyncedTree<T, L: TreeListener> {
    tree: Arc<Tree<T>>,
    model: L,
}

impl<T: Clone + PartialEq, L: TreeListener> SyncedTree<T, L> {
    pub fn new(model: L) -> SyncedTree<T, L> {
        SyncedTree {
            tree: Arc::default(),
            model,
        }
    }
    pub fn apply(&mut self, change: TreeChange<T>) {
        match change {
            TreeChange::Reset(tree) => {
                // avoid unneeded model reset
                if self.tree != tree {
                    self.model.begin_reset_model();
                    self.tree = tree;
                    self.model.end_reset_model();
                }
            }
            TreeChange::Insert(parent, pos, t) => {
                self.model.begin_insert_rows(parent, pos, pos);
                Arc::make_mut(&mut self.tree).insert(parent, pos, t);
                self.model.end_insert_rows();
            }
        }
    }
    pub fn row_count(&self, index: Option<usize>) -> usize {
        self.tree.row_count(index)
    }
    pub fn index(&self, index: Option<usize>, row: usize) -> usize {
        self.tree.index(index, row)
    }
    pub fn parent(&self, index: usize) -> Option<usize> {
        self.tree.parent(index)
    }
    pub fn row(&self, index: usize) -> usize {
        self.tree.row(index)
    }
}
