use crate::message::Message;
use mailparse::{parse_mail, ParsedMail};
use std::io::{self, Read, Write};
use std::process::{Child, Command, ExitStatus, Stdio};
use std::thread::sleep;
use std::time::{Duration, Instant};
use tempfile::NamedTempFile;

pub enum SignatureCheck {
    NoSignature,
    #[allow(dead_code)]
    SignatureOk,
    #[allow(dead_code)]
    SignatureInvalid,
}

/// try to decrypt a message
/// this function can show a dialog to the user
pub fn decrypt(message: Message) -> (Message, SignatureCheck) {
    let parsed = match parse_mail(&message.data) {
        Err(_) => return (message, SignatureCheck::NoSignature),
        Ok(parsed) => parsed,
    };
    if let Some(encrypted_asc) = get_encrypted_asc(&parsed) {
        match decrypt_encrypted_asc(&encrypted_asc, true) {
            Ok(data) => {
                let parsed = match parse_mail(&data) {
                    Err(e) => {
                        eprintln!("Could not parse decrypted message {}", e);
                        return (message, SignatureCheck::NoSignature);
                    }
                    Ok(parsed) => parsed,
                };
                check_signature(&parsed, &data);
                match Message::decrypted(&message, data.clone()) {
                    Ok(message) => {
                        eprintln!("managed to decrypt message");
                        (message, SignatureCheck::NoSignature)
                    }
                    Err(e) => {
                        eprintln!("could not parse decrypted message {:?}", e);
                        (message, SignatureCheck::NoSignature)
                    }
                }
            }
            Err(e) => {
                eprintln!("could not decrypt message {:?}", e);
                (message, SignatureCheck::NoSignature)
            }
        }
    } else {
        check_signature(&parsed, &message.data);
        (message, SignatureCheck::NoSignature)
    }
}

pub fn find_subsequence(haystack: &[u8], needle: &[u8]) -> Option<usize> {
    haystack
        .windows(needle.len())
        .position(|window| window == needle)
}

fn make_boundary(boundary: &[u8], eol: &[u8]) -> Vec<u8> {
    let mut b = Vec::new();
    b.extend_from_slice(eol);
    b.extend_from_slice(b"--");
    b.extend_from_slice(boundary);
    b.extend_from_slice(eol);
    b
}

fn get_first_part<'a>(data: &'a [u8], boundary: &[u8]) -> Option<&'a [u8]> {
    let b = make_boundary(boundary, b"\n");
    if let Some(pos) = find_subsequence(data, &b) {
        let data = &data[pos + b.len()..];
        if let Some(end) = find_subsequence(data, &b) {
            return Some(&data[..end]);
        }
    } else {
        let b = make_boundary(boundary, b"\r\n");
        if let Some(pos) = find_subsequence(data, &b) {
            let data = &data[pos + b.len()..];
            if let Some(end) = find_subsequence(data, &b) {
                return Some(&data[..end]);
            }
        }
    }
    None
}

fn check_signature(part: &ParsedMail, data: &[u8]) -> SignatureCheck {
    if part.ctype.mimetype != "multipart/signed"
        || !part.ctype.params.contains_key("boundary")
        //|| !part.ctype.params.contains_key("micalg")
        || part.subparts.len() != 2
        || part.subparts[1].ctype.mimetype != "application/pgp-signature"
    {
        return SignatureCheck::NoSignature;
    }
    let boundary = part.ctype.params["boundary"].as_bytes();
    let data = match get_first_part(data, boundary) {
        None => {
            println!("{}", String::from_utf8_lossy(data));
            println!("Could not check signature");
            return SignatureCheck::NoSignature;
        }
        Some(data) => data,
    };
    if let Ok(signature) = part.subparts[1].get_body_raw() {
        let _ = check_signature2(data, &signature);
        SignatureCheck::NoSignature
    } else {
        eprintln!("Could not check signature");
        SignatureCheck::NoSignature
    }
}

fn get_encrypted_asc(part: &ParsedMail) -> Option<Vec<u8>> {
    if let Some(body) = get_attachment("encrypted.asc", &part) {
        return Some(body);
    }
    get_pgp_encrypted(&part)
}

/// look for the first attachment with the given name
fn get_attachment<'a>(name: &str, part: &ParsedMail<'a>) -> Option<Vec<u8>> {
    let n = part.ctype.params.get("name");
    if let (Some(n), Ok(data)) = (n, part.get_body_raw()) {
        if n == name {
            return Some(data);
        }
    }
    for part in &part.subparts {
        if let Some(msg) = get_attachment(name, part) {
            return Some(msg);
        }
    }
    None
}

// get the encrypted body for PGP encrypted messages with application/pgp-encrypted
// https://tools.ietf.org/html/rfc1847
fn get_pgp_encrypted(part: &ParsedMail) -> Option<Vec<u8>> {
    if part.ctype.mimetype != "multipart/encrypted"
        || part.ctype.params.get("protocol").map(|s| s.as_str())
            != Some("application/pgp-encrypted")
    {
        return None;
    }
    if part.subparts.len() < 2
        || part.subparts[0].ctype.mimetype != "application/pgp-encrypted"
        || part.subparts[1].ctype.mimetype != "application/octet-stream"
    {
        eprintln!("Unsupported encrypted mail");
    }
    if let Ok(data) = part.subparts[1].get_body_raw() {
        return Some(data);
    }
    None
}

fn wait_with_timeout(p: &mut Child, timeout: Duration) -> io::Result<ExitStatus> {
    let sleep_time = Duration::from_millis(1);
    let start = Instant::now();
    loop {
        match p.try_wait()? {
            None => {
                sleep(sleep_time);
                if start.elapsed() > timeout {
                    let _ = p.kill();
                    return Err(io::Error::new(
                        io::ErrorKind::TimedOut,
                        "Decrypting the message timed out.",
                    ));
                }
            }
            Some(exit) => return Ok(exit),
        }
    }
}

fn run_for_stdout(p: &mut Child, to_stdin: &[u8], timeout: Duration) -> io::Result<Vec<u8>> {
    let mut stdin = p.stdin.take().unwrap();
    stdin.write_all(to_stdin)?;
    drop(stdin);
    wait_with_timeout(p, timeout)?;
    let mut stdout = p.stdout.take().unwrap();
    let mut data = Vec::new();
    stdout.read_to_end(&mut data)?;
    Ok(data)
}

fn decrypt_encrypted_asc(encrypted_asc: &[u8], interactive: bool) -> io::Result<Vec<u8>> {
    let mut p = Command::new("gpg");
    if interactive {
        if std::env::var("DISPLAY").is_err() {
            return Err(io::Error::new(
                io::ErrorKind::NotConnected,
                "Cannot show gpg dialog because DISPLAY is not set.",
            ));
        }
        p.arg("--no-batch");
    } else {
        p.arg("--batch");
    }
    let mut p = p
        .arg("--verbose")
        .arg("--exit-on-status-write-error")
        .arg("--decrypt")
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn()?;
    run_for_stdout(&mut p, encrypted_asc, Duration::from_millis(5000))
}

fn check_signature2(data: &[u8], signature: &[u8]) -> io::Result<()> {
    let mut file = NamedTempFile::new()?;
    file.write_all(data)?;
    let mut p = Command::new("gpg")
        .arg("--verbose")
        .arg("--exit-on-status-write-error")
        .arg("--with-colons")
        .arg("--status-fd")
        .arg("1")
        .arg("--verify")
        .arg("-")
        .arg(file.path())
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn()?;
    let out = run_for_stdout(&mut p, signature, Duration::from_millis(5000))?;
    println!("{}", String::from_utf8_lossy(&out));
    // TODO: parse stdout, e.g.
    //[GNUPG:] NEWSIG
    //[GNUPG:] KEY_CONSIDERED 837DFE6CAC0003082B09647C91BF7E1D5055FBBF 0
    //[GNUPG:] KEY_CONSIDERED 837DFE6CAC0003082B09647C91BF7E1D5055FBBF 0
    //[GNUPG:] BADSIG 91BF7E1D5055FBBF Jos van den Oever <jos@vandenoever.info>
    //[GNUPG:] VERIFICATION_COMPLIANCE_MODE 23
    //
    Ok(())
}
