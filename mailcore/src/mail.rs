use crate::errors::{ErrorKind, Result};
use crate::job_processor::{
    EmailReceiver, FolderReceiver, FoldersReceiver, Job, JobProcessor, Observer,
};
use crate::mail_store::{EventHandler, MailStoreChange};
use crate::message::{Message, Uid};
use crate::sync_tree::TreeChange;
use crate::Folder;
use std::path::Path;
use std::sync::Arc;

/// Interface to a mail store
pub struct Mail {
    job_processor:
        JobProcessor<Box<dyn FoldersReceiver>, Box<dyn FolderReceiver>, Box<dyn EmailReceiver>>,
}

impl Mail {
    pub fn create<P: AsRef<Path>>(config_file: P) -> Result<Mail> {
        let job_processor = JobProcessor::create(config_file)?;
        Ok(Mail { job_processor })
    }
    pub fn observe_folders<R: FoldersReceiver>(&self, folders_receiver: R) -> Result<()> {
        self.job_processor
            .send(Job::GetFolders(Box::new(folders_receiver)))
            .map_err(|_| ErrorKind::MailThreadDied)?;
        Ok(())
    }
    pub fn observe_folder<R: FolderReceiver>(
        &self,
        folder: &str,
        folder_receiver: R,
    ) -> Result<()> {
        self.job_processor
            .send(Job::SetFolder(Box::new(folder_receiver), folder.into()))
            .map_err(|_| ErrorKind::MailThreadDied)?;
        Ok(())
    }
    pub fn get_email<R: EmailReceiver>(
        &self,
        folder: &str,
        uid: Uid,
        email_receiver: R,
    ) -> Result<()> {
        self.job_processor
            .send(Job::SetEmail(Box::new(email_receiver), folder.into(), uid))
            .map_err(|_| ErrorKind::MailThreadDied)?;
        Ok(())
    }
}

impl Observer for Box<dyn FoldersReceiver> {
    fn done(&self) -> bool {
        (**self).done()
    }
}

impl FoldersReceiver for Box<dyn FoldersReceiver> {
    fn set_folders(&self, folders: Vec<Folder>) {
        (**self).set_folders(folders)
    }
}

impl Observer for Box<dyn FolderReceiver> {
    fn done(&self) -> bool {
        (**self).done()
    }
}

impl FolderReceiver for Box<dyn FolderReceiver> {
    fn add_changes(&mut self, changes: Vec<TreeChange<Arc<Message>>>) {
        (**self).add_changes(changes)
    }
}

impl EmailReceiver for Box<dyn EmailReceiver> {
    fn set_email(&self, email: Option<Arc<Message>>) {
        (**self).set_email(email)
    }
}

impl EventHandler for Box<dyn EventHandler> {
    fn handle_event(&mut self, event: MailStoreChange) {
        (**self).handle_event(event)
    }
}
