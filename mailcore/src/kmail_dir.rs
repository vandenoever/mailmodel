use std::path::{Path, PathBuf};
use walkdir::{DirEntry, WalkDir};

// Does the directory name match the glob pattern '.*.directory'?
// For directory '.X.directory' there should be a sibling directory 'X'.
fn is_dot_directory(path: &Path) -> bool {
    let name = path.file_name().and_then(|name| name.to_str());
    if let Some(name) = name {
        if name.len() > 11 && name.starts_with('.') && name.ends_with(".directory") {
            if let Some(parent) = path.parent() {
                let related_dir_name = &name[1..name.len() - 10];
                if let Ok(metadata) = parent.join(related_dir_name).metadata() {
                    return metadata.is_dir();
                }
            }
        }
    }
    false
}

/// Iterate over all the . directories
fn get_dot_directories<P: AsRef<Path>>(path: P) -> impl Iterator<Item = DirEntry> {
    WalkDir::new(path)
        .sort_by(|a, b| a.path().cmp(b.path()))
        .into_iter()
        .filter_entry(|e| e.file_type().is_dir() && (e.depth() == 0 || is_dot_directory(e.path())))
        .filter_map(|e| e.ok())
        .filter(|e| e.depth() != 0)
}

/// list all subdirectories that are not dot directories
fn iter_mail_dirs<P: AsRef<Path>>(
    path: P,
) -> std::io::Result<impl Iterator<Item = std::fs::DirEntry>> {
    Ok(std::fs::read_dir(path)?
        .filter_map(|e| e.ok())
        .filter(|e| e.metadata().map(|m| m.file_type().is_dir()).ok() == Some(true))
        .filter(|e| !is_dot_directory(&e.path())))
}

pub fn get_mail_dirs<P: AsRef<Path>>(path: P) -> Vec<MailDir> {
    let path = path.as_ref();
    let mut mail_dirs = Vec::new();
    if let Some(mail_dir) = MailDir::new(path, path) {
        mail_dirs.push(mail_dir);
    }
    if let Ok(iter) = iter_mail_dirs(path) {
        for dir in iter
            .filter(|e| e.file_name() != "new" && e.file_name() != "tmp" && e.file_name() != "cur")
        {
            if let Some(mail_dir) = MailDir::new(path, &dir.path()) {
                mail_dirs.push(mail_dir);
            }
        }
    }
    for dots in get_dot_directories(path) {
        if let Ok(iter) = iter_mail_dirs(dots.path()) {
            for dir in iter {
                if let Some(mail_dir) = MailDir::new(path, &dir.path()) {
                    mail_dirs.push(mail_dir);
                }
            }
        }
    }
    mail_dirs
}

/// Turn a kmail folder path to the name that you can be use
/// in user interface.
/// .a.directory/b becomes a/b
fn mail_folder_path_to_name(path: &Path) -> Option<String> {
    let mut name = String::new();
    if let Some(parent) = path.parent() {
        for n in parent.iter() {
            if let Some(n) = n.to_str() {
                let len = n.len();
                assert!(len > 11);
                assert!(&n[0..1] == ".");
                assert!(&n[len - 10..] == ".directory");
                name.push_str(&n[1..len - 10]);
                name.push('/');
            } else {
                return None;
            }
        }
    }
    if let Some(file_name) = path.file_name() {
        if let Some(file_name) = file_name.to_str() {
            name.push_str(file_name);
            Some(name)
        } else {
            None
        }
    } else {
        Some(String::new())
    }
}

#[test]
fn test_mail_folder_path_to_name_1() {
    fn ok(a: &str, b: &str) {
        assert_eq!(
            mail_folder_path_to_name(&PathBuf::from(a)),
            Some(String::from(b))
        );
    }
    fn none(a: &[u8]) {
        use std::os::unix::ffi::OsStrExt;
        let osstr = std::ffi::OsStr::from_bytes(a);
        assert_eq!(mail_folder_path_to_name(&PathBuf::from(osstr)), None);
    }
    ok("", "");
    ok("a", "a");
    ok(".b.directory/a", "b/a");
    ok(".c.directory/.b.directory/a", "c/b/a");
    none(b"\xff");
}

#[test]
#[should_panic]
fn test_mail_folder_path_to_name_2() {
    // a does not fit the .*.directory pattern
    mail_folder_path_to_name(&PathBuf::from("a/b"));
}

#[derive(Clone, PartialEq)]
pub struct MailDir {
    pub path: PathBuf,
    pub name: String,
}

impl MailDir {
    #[allow(clippy::new_ret_no_self)]
    fn new(base: &Path, path: &Path) -> Option<MailDir> {
        if let Ok(rel_path) = path.strip_prefix(base) {
            if let Some(name) = mail_folder_path_to_name(rel_path) {
                Some(MailDir {
                    path: rel_path.into(),
                    name,
                })
            } else {
                None
            }
        } else {
            None
        }
    }
}

#[test]
fn list_dirs() {
    let dirs = get_mail_dirs("testdata/mail");
    assert!(!dirs.is_empty());
    assert_eq!(dirs.len(), 2);
}
