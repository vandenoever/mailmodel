use crate::errors::{ErrorKind, Result, ResultExt};
use crate::kmail_dir;
use crate::mail_flags::MailFlags;
use crate::mail_store::{Folder, Name};
use crate::message::{Message, Uid};
use chrono::offset::FixedOffset;
use chrono::{DateTime, NaiveDateTime};
use rusqlite::types::ToSql;
use rusqlite::{CachedStatement, Connection, Row, Statement};
use sha2::digest::generic_array::GenericArray;
use std::collections::BTreeMap;
use std::path::{Path, PathBuf};
use typenum::consts::U32;

pub struct Db<'a> {
    db: &'a Connection,
    list_mail_folders: Statement<'a>,
    list_mail_files: Statement<'a>,
    insert_blob: Statement<'a>,
    insert_mail_folder: Statement<'a>,
    insert_mail_file: Statement<'a>,
    insert_mail: Statement<'a>,
    find_blob: Statement<'a>,
    delete_mail_folder: Statement<'a>,
    delete_mail_file: Statement<'a>,
    delete_mail_file_by_path: Statement<'a>,
    clean_up_mails_and_blobs: Statement<'a>,
}

const LIST_MAIL_FOLDERS: &str = "SELECT id, path, name FROM mail_folder ORDER BY name";
const LIST_MAIL_FOLDERS_WITH_COUNTS: &str =
    "SELECT mail_folder.id, name, ifnull(sum(seen), 0) as seen, count(seen)
     FROM mail_folder
     LEFT JOIN mail_file ON mail_folder.id = mail_file.folder
     LEFT JOIN mail ON mail_file.blob = mail.id
     GROUP BY mail_folder.id";
const LIST_MAILS: &str =
    "SELECT f.id, subject, from_, message_id, in_reply_to, mail_folder.path, seen, new, f.file_name, date
     FROM mail_folder
     JOIN mail_file AS f ON mail_folder.id = f.folder
     JOIN mail ON f.blob = mail.id
     WHERE mail_folder.name = ?1
     ORDER BY date";
const READ_MAIL: &str = "SELECT mail_folder.path, seen, new, f.file_name
     FROM mail_folder
     JOIN mail_file AS f ON mail_folder.id = f.folder
     JOIN mail ON f.blob = mail.id
     WHERE f.id = ?1
     ORDER BY date";

impl<'a> Db<'a> {
    #[allow(clippy::new_ret_no_self)]
    pub fn new(db: &'a Connection) -> Result<Db> {
        let list_mail_folders = db.prepare(LIST_MAIL_FOLDERS)?;
        let list_mail_files = db.prepare(
            "SELECT mail_file.id, file_name, seen, new, mtime, blob.length
             FROM mail_file
             LEFT JOIN blob ON mail_file.blob = blob.id
             WHERE folder = ?1
             ORDER BY new ASC, file_name ASC",
        )?;
        let insert_blob = db.prepare("INSERT INTO blob (length, sha256) VALUES(?1, ?2)")?;
        let insert_mail_folder =
            db.prepare("INSERT INTO mail_folder (path, name) VALUES(?1, ?2)")?;
        let insert_mail_file = db.prepare(
            "INSERT INTO mail_file (folder, seen, new, file_name, mtime, blob) VALUES(?1, ?2, ?3, ?4, ?5, ?6)",
        )?;
        let insert_mail = db.prepare(
            "INSERT INTO mail
             (id, subject, from_, message_id, in_reply_to, date)
             VALUES(?1, ?2, ?3, ?4, ?5, ?6)",
        )?;
        let find_blob = db.prepare(
            "SELECT blob.id, mail.id FROM blob
             LEFT JOIN mail ON blob.id = mail.id
             WHERE sha256 = ?1 LIMIT 1",
        )?;
        let delete_mail_file = db.prepare("DELETE FROM mail_file WHERE id = ?1")?;
        let delete_mail_file_by_path =
            db.prepare("DELETE FROM mail_file WHERE folder = ?1 AND seen = ?2 AND file_name = ?3")?;
        let delete_mail_folder = db.prepare("DELETE FROM mail_folder WHERE id = ?1")?;
        let clean_up_mails_and_blobs = db.prepare(
            "DELETE FROM mail WHERE NOT EXISTS (
                 SELECT blob FROM mail_file WHERE blob = mail.id
             );
             DELETE FROM blob WHERE NOT EXISTS (
                 SELECT blob FROM mail_file WHERE blob = blob.id
             );",
        )?;
        Ok(Db {
            db,
            list_mail_folders,
            list_mail_files,
            insert_blob,
            insert_mail_folder,
            insert_mail_file,
            insert_mail,
            find_blob,
            delete_mail_file,
            delete_mail_file_by_path,
            delete_mail_folder,
            clean_up_mails_and_blobs,
        })
    }
    fn list_mail_folders_closure(row: &Row) -> rusqlite::Result<(i64, kmail_dir::MailDir)> {
        let path: String = row.get(1)?;
        let id = row.get(0)?;
        Ok((
            id,
            kmail_dir::MailDir {
                path: path.into(),
                name: row.get(2)?,
            },
        ))
    }
    pub fn list_mail_folders(&mut self) -> Result<BTreeMap<i64, kmail_dir::MailDir>> {
        let folders = self
            .list_mail_folders
            .query_map([], Db::list_mail_folders_closure)
            .unwrap()
            .filter_map(|f| f.ok())
            .collect();
        Ok(folders)
    }
    pub fn list_mail_folders_with_counts(c: &Connection) -> Result<BTreeMap<i64, Folder>> {
        let mut list_mail_folders = c.prepare_cached(LIST_MAIL_FOLDERS_WITH_COUNTS)?;
        let folders = list_mail_folders
            .query_map([], |row| {
                let messages = row.get::<_, i64>(3)? as usize;
                // SELECT mail_folder.id, name, sum(seen), count(*)
                Ok((
                    row.get(0)?,
                    Folder {
                        name: Name {
                            delimiter: String::from("/"),
                            name: row.get(1)?,
                        },
                        messages: Some(messages),
                        unread: Some(messages - row.get::<_, i64>(2)? as usize),
                    },
                ))
            })
            .unwrap()
            .filter_map(|f| f.ok())
            .collect();
        Ok(folders)
    }
    pub fn delete_mail_folder(&mut self, id: i64) -> Result<()> {
        self.delete_mail_folder.execute(&[&id])?;
        Ok(())
    }
    pub fn insert_mail_folder(&mut self, path: &Path, name: &str) -> Result<i64> {
        let path: String = path.to_str().unwrap().to_string();
        self.insert_mail_folder
            .execute(&[&path as &dyn ToSql, &name])
            .chain_err(|| {
                ErrorKind::DbInsert(format!("Error inserting mail folder '{}' '{}'", path, name))
            })?;
        // get the id for the blob
        Ok(self.db.last_insert_rowid())
    }
    pub fn delete_mails(&mut self, ids: Vec<i64>) -> Result<()> {
        for id in ids {
            self.delete_mail_file.execute(&[&id])?;
        }
        Ok(())
    }
    pub fn delete_mail_file_by_path(
        &mut self,
        folder: i64,
        seen: bool,
        file_name: &str,
    ) -> Result<()> {
        let deleted =
            self.delete_mail_file_by_path
                .execute(&[&folder as &dyn ToSql, &seen, &file_name])?;
        assert_eq!(deleted, 1);
        Ok(())
    }
    pub fn clean_up_mails_and_blobs(&mut self) -> Result<()> {
        self.clean_up_mails_and_blobs.execute([])?;
        Ok(())
    }
    fn add_blob(&mut self, sha256: GenericArray<u8, U32>, length: u64) -> Result<i64> {
        let length = length as i64;
        let blob_id = match self
            .insert_blob
            .execute(&[&length as &dyn ToSql, &sha256.as_slice()])
        {
            Ok(_) => {
                // get the id for the blob
                self.db.last_insert_rowid()
            }
            Err(rusqlite::Error::SqliteFailure(e, _))
                if e.code == libsqlite3_sys::ErrorCode::ConstraintViolation =>
            {
                // blob already in this table, find the id
                let mut rows = self.find_blob.query(&[sha256.as_slice()])?;
                rows.next()?.unwrap().get::<_, i64>(0)?
            }
            Err(e) => Err(e)?,
        };
        Ok(blob_id)
    }
    pub fn add_mail(
        &mut self,
        mail: &MailFileRow,
        sha256: GenericArray<u8, U32>,
        folder_id: i64,
        message: Option<Message>,
    ) -> Result<()> {
        let blob_id = self.add_blob(sha256, mail.length)?;
        // save the mail file
        let mtime = mail.mtime as i64;
        self.insert_mail_file.execute(&[
            &folder_id as &dyn ToSql,
            &mail.seen,
            &mail.new,
            &mail.file_name,
            &mtime as &dyn ToSql,
            &blob_id,
        ])?;
        // save the mail information
        if let Some(m) = message {
            // (id, subject, from_, message_id, in_reply_to, date)
            let date = m.date.naive_utc().timestamp();
            let r = self.insert_mail.execute(&[
                &blob_id as &dyn ToSql,
                &m.subject,
                &m.from,
                &m.message_id,
                &m.in_reply_to,
                &date,
            ]);
            match r {
                Err(rusqlite::Error::SqliteFailure(e, _))
                    if e.code == libsqlite3_sys::ErrorCode::ConstraintViolation =>
                {
                    // mail already in this table, no problem
                }
                Err(e) => Err(e)?,
                Ok(_) => {}
            }
        }
        Ok(())
    }
    pub fn list_mail_files(
        &mut self,
        folder_id: i64,
    ) -> Result<impl Iterator<Item = MailFileRow> + '_> {
        let rows = self
            .list_mail_files
            .query_map([folder_id], |row| {
                Ok(MailFileRow {
                    id: row.get(0)?,
                    file_name: row.get(1)?,
                    seen: row.get::<_, bool>(2)?,
                    new: row.get::<_, bool>(3)?,
                    mtime: row.get::<_, i64>(4)? as u64,
                    length: row.get::<_, i64>(5)? as u64,
                })
            })?
            .filter_map(|e| e.ok());
        Ok(rows)
    }
    pub fn mail_lister(c: &Connection, mail_dir: PathBuf) -> Result<MailLister> {
        let list_mails = c.prepare_cached(LIST_MAILS)?;
        Ok(MailLister {
            stmt: list_mails,
            mail_dir,
        })
    }
    pub fn read_mail(c: &Connection, mail_dir: &Path, uid: Uid) -> Result<Message> {
        let mut read_mail = c.prepare_cached(READ_MAIL)?;
        let (path, seen, new, file_name) = read_mail
            .query_map([uid.0 as i64], |row| {
                let path: String = row.get(0)?;
                let seen: bool = row.get(1)?;
                let new: bool = row.get(2)?;
                let file_name: String = row.get(3)?;
                Ok((path, seen, new, file_name))
            })
            .unwrap()
            .filter_map(|f| f.ok())
            .next()
            .unwrap();
        let path = get_mail_path(mail_dir, &path, new, &file_name);
        let bytes = std::fs::read(&path)?;
        Ok(Message::from_bytes(
            uid,
            Some(MailFlags::from_seen(seen)),
            bytes,
            Some(path),
        )?)
    }
}

fn get_mail_path(mail_dir: &Path, path: &str, new: bool, file_name: &str) -> PathBuf {
    mail_dir
        .join(path)
        .join(if new { "new" } else { "cur" })
        .join(file_name)
}

pub struct MailLister<'a> {
    stmt: CachedStatement<'a>,
    mail_dir: PathBuf,
}

impl<'a> MailLister<'a> {
    pub fn list_mails<'b>(
        &'b mut self,
        folder: &str,
    ) -> Result<impl Iterator<Item = Message> + 'b> {
        let mail_dir = self.mail_dir.clone();
        let rows = self
            .stmt
            .query_map(&[folder], move |row| {
                let path: String = row.get(5)?;
                let new: bool = row.get(7)?;
                let file_name: String = row.get(8)?;
                let path = get_mail_path(&mail_dir, &path, new, &file_name);
                let date = NaiveDateTime::from_timestamp(row.get::<_, i64>(9)?, 0);
                let date = DateTime::from_utc(date, FixedOffset::west(0));
                Ok(Message {
                    uid: Uid(row.get::<_, i64>(0)? as usize),
                    subject: row.get(1)?,
                    from: row.get(2)?,
                    message_id: row.get(3)?,
                    in_reply_to: row.get(4)?,
                    flags: Some(MailFlags::from_seen(row.get(6)?)),
                    date,
                    thread_end: date,
                    data: Vec::new(),
                    file_path: Some(path),
                })
            })?
            .filter_map(|e| {
                if let Err(e) = &e {
                    eprintln!("{:?}", e);
                }
                e.ok()
            });
        Ok(rows)
    }
}

pub struct MailFileRow {
    pub id: i64,
    pub file_name: String,
    pub seen: bool,
    pub new: bool,
    pub mtime: u64, // seconds since the epoch utc
    pub length: u64,
}
