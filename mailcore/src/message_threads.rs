use crate::message::Message;
use crate::sync_tree::{LoggingTree, TreeChange};
use crate::tree::Tree;
use chrono::offset::FixedOffset;
use chrono::DateTime;
use std::collections::HashMap;
use std::sync::Arc;

#[derive(Clone, PartialEq, Default)]
pub struct MessageThreads {
    tree: Arc<Tree<Arc<Message>>>,
    messages: HashMap<String, usize>,
}

impl MessageThreads {
    pub fn len(&self) -> usize {
        self.tree.len()
    }
    pub fn tree(&self) -> &Arc<Tree<Arc<Message>>> {
        &self.tree
    }
}

pub struct MessageThreader {
    tree: LoggingTree<Arc<Message>>,
    messages: HashMap<String, usize>,
}

impl MessageThreader {
    pub fn new(
        threads: &MessageThreads,
        tree: Arc<Tree<Arc<Message>>>,
        should_record: bool,
    ) -> MessageThreader {
        let mut tree = LoggingTree::new(tree, should_record);
        tree.reset(threads.tree.clone());
        MessageThreader {
            tree,
            messages: threads.messages.clone(),
        }
    }
    pub fn len(&self) -> usize {
        self.tree.len()
    }
    // return true if thread_end was changed
    fn set_thread_end(&mut self, mut index: Option<usize>, date: DateTime<FixedOffset>) -> bool {
        let mut change = false;
        while let Some(i) = index {
            if self.tree[i].thread_end < date {
                Arc::make_mut(self.tree.borrow_for_non_data_change(i)).thread_end = date;
                change = true;
            }
            index = self.tree.parent(i);
        }
        change
    }
    pub fn add_message(&mut self, message: Arc<Message>) {
        // does the message have references and are they in this threader?
        // for now, just check 'in-reply-to'
        let mut parent = None;
        if let Some(in_reply_to) = &message.in_reply_to {
            if let Some(p) = self.messages.get(in_reply_to) {
                parent = Some(*p);
            }
        }
        self.set_thread_end(parent, message.date);
        let message_id = message.message_id.to_owned();
        let index = self.tree.append(parent, message);
        self.messages.entry(message_id).or_insert(index);
    }
    pub fn done(self) -> (Vec<TreeChange<Arc<Message>>>, MessageThreads) {
        let mt = MessageThreads {
            tree: self.tree.tree().clone(),
            messages: self.messages,
        };
        (self.tree.log(), mt)
    }
}
