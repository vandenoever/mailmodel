#![recursion_limit = "128"]

#[macro_use]
extern crate error_chain;

mod db;
mod gpg;
mod imap_store;
mod indexed_mail_dir_store;
mod job_processor;
mod kmail_dir;
pub mod mail;
mod mail_dir_index;
mod mail_dir_store;
mod mail_dir_watcher;
mod mail_flags;
mod mail_store;
mod mail_store_state;
pub mod message;
mod message_threads;
pub mod new_message;
pub mod sorted_filtered_tree;
pub mod sync_tree;
mod tree;

pub use crate::indexed_mail_dir_store::IndexedMailDirConfig;
pub use crate::job_processor::{EmailReceiver, FolderReceiver, FoldersReceiver, Observer};
pub use crate::mail::Mail;
pub use crate::mail_flags::MailFlags;
pub use crate::mail_store::{EventHandler, Folder, MailStoreChange, MailStoreConfig};

mod errors {
    use std::path::PathBuf;

    // Create the Error, ErrorKind, ResultExt, and Result types
    error_chain! {
        foreign_links {
            Io(std::io::Error);
            Imap(imap::error::Error);
            Tls(native_tls::Error);
            Tls2(native_tls::HandshakeError<std::net::TcpStream>);
            Serde(serde_json::Error);
            StripPrefixError(std::path::StripPrefixError);
            Chrono(chrono::ParseError);
            MailParse(mailparse::MailParseError);
            Rusqlite(rusqlite::Error);
            Notify(notify::Error);
        }
        errors {
            InvalidConfiguration(t: String) {
                description("invalid configuration")
                display("invalid configuration: {}", t)
            }
            FileNotFound(path: PathBuf) {
                description("file not found")
                display("file not found: {}", path.display())
            }
            MailThreadDied {
                description("the mail thread died")
                display("the mail thread died")
            }
            DbInsert(t: String) {
                description("error inserting into the database")
                display("error inserting into the database")
            }
            WatcherStopped {
                description("the watch was stopped")
                display("the watch was stopped")
            }
            Nom(kind: nom::error::ErrorKind) {
                description("parsing error")
                display("parsing error: {:?}", kind)
            }
        }
    }

    impl From<nom::Err<nom::error::Error<&[u8]>>> for Error {
        fn from(err: nom::Err<nom::error::Error<&[u8]>>) -> Error {
            let kind = match err {
                nom::Err::Incomplete(_) => nom::error::ErrorKind::Eof,
                nom::Err::Error(e) => e.code,
                nom::Err::Failure(e) => e.code,
            };
            Error::from_kind(ErrorKind::Nom(kind))
        }
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
