#![allow(dead_code)]
use crate::db;
use crate::errors::{ErrorKind, Result};
use crate::mail_dir_index::Db;
use crate::mail_dir_watcher::MailDirWatcher;
use crate::mail_store::{EventHandler, Folder, MailStore};
use crate::mail_store_state::MailStoreState;
use crate::message::{Message, Uid};
use crate::message_threads::MessageThreader;
use rusqlite::Connection;
use serde_derive::{Deserialize, Serialize};
use std::collections::HashSet;
use std::path::PathBuf;
use std::sync::{Arc, Mutex};

#[derive(Deserialize, Clone, Serialize, Debug)]
pub struct IndexedMailDirConfig {
    pub mail_dir: PathBuf,
    pub index_file: PathBuf,
}

pub struct IndexedMailDirStore {
    config: IndexedMailDirConfig,
    connection: Connection,
    watcher: MailDirWatcher,
    done: HashSet<String>,
}

impl IndexedMailDirStore {
    #[allow(clippy::new_ret_no_self)]
    pub fn new<H: EventHandler>(
        config: IndexedMailDirConfig,
        event_sink: Arc<Mutex<H>>,
    ) -> Result<IndexedMailDirStore> {
        if config.index_file.starts_with(&config.mail_dir) {
            return Err(ErrorKind::InvalidConfiguration(format!(
                "index_file ({}) should not be in mail_dir ({})",
                config.index_file.display(),
                config.mail_dir.display()
            ))
            .into());
        }
        if !config.mail_dir.is_dir() {
            return Err(ErrorKind::InvalidConfiguration(format!(
                "mail_dir ({}) does not exist",
                config.mail_dir.display()
            ))
            .into());
        }
        let connection = db::open(&config.index_file)?;
        Ok(IndexedMailDirStore {
            config: config.clone(),
            connection,
            watcher: MailDirWatcher::new(config, event_sink)?,
            done: HashSet::new(),
        })
    }
}

impl MailStore for IndexedMailDirStore {
    fn is_folder_complete(&self, folder: &str, _: &MailStoreState) -> bool {
        self.done.contains(folder)
    }
    fn list_folders(&mut self) -> Result<Vec<Folder>> {
        let folders = Db::list_mail_folders_with_counts(&self.connection)?;
        let folders: Vec<Folder> = folders
            .into_iter()
            .map(|f| f.1)
            .filter(|f| !f.name.name.is_empty())
            .collect();
        Ok(folders)
    }
    fn fetch_headers(&mut self, folder: &str, threads: &mut MessageThreader) -> Result<()> {
        let mut lister = Db::mail_lister(&self.connection, self.config.mail_dir.clone())?;
        for m in lister.list_mails(folder)? {
            threads.add_message(Arc::new(m));
        }
        self.done.insert(folder.to_string());
        Ok(())
    }
    fn fetch(&mut self, _folder: &str, uid: Uid) -> Result<Message> {
        Ok(Db::read_mail(&self.connection, &self.config.mail_dir, uid)?)
    }
}
