use crate::errors::{ErrorKind, Result, ResultExt};
use crate::imap_store::{IMAPConfig, IMAPStore};
use crate::indexed_mail_dir_store::{IndexedMailDirConfig, IndexedMailDirStore};
use crate::mail_dir_store::{MailDirConfig, MailDirStore};
use crate::mail_store_state::MailStoreState;
use crate::message::{Message, Uid};
use crate::message_threads::MessageThreader;
use serde_derive::{Deserialize, Serialize};
use std::fs;
use std::path::Path;
use std::sync::{Arc, Mutex};

#[derive(Debug, Clone, PartialEq)]
pub struct Name {
    pub delimiter: String,
    pub name: String,
}

#[derive(Debug, Clone, PartialEq)]
pub struct Folder {
    pub name: Name,
    pub messages: Option<usize>,
    pub unread: Option<usize>,
}

#[derive(Deserialize, Debug, Serialize)]
pub enum MailStoreConfig {
    IMAP(IMAPConfig),
    MailDir(MailDirConfig),
    IndexedMailDir(IndexedMailDirConfig),
}

pub trait MailStore: Send {
    fn is_folder_complete(&self, folder: &str, state: &MailStoreState) -> bool;
    fn list_folders(&mut self) -> Result<Vec<Folder>>;
    fn fetch(&mut self, folder: &str, uid: Uid) -> Result<Message>;
    fn fetch_headers(&mut self, folder: &str, threads: &mut MessageThreader) -> Result<()>;
}

#[derive(Debug, PartialEq)]
pub enum MailStoreChange {
    FolderChanged(Name),
    FoldersListChanged,
}

pub trait EventHandler: Send + 'static {
    fn handle_event(&mut self, _: MailStoreChange);
}

pub fn create_mail_store<E: EventHandler, P: AsRef<Path>>(
    path: P,
    event_sink: Arc<Mutex<E>>,
) -> Result<Box<dyn MailStore>> {
    let contents = fs::read_to_string(path.as_ref())
        .chain_err(|| ErrorKind::FileNotFound(path.as_ref().to_path_buf()))?;
    let config: MailStoreConfig = serde_json::from_str(&contents)?;
    Ok(match config {
        MailStoreConfig::IMAP(config) => Box::new(IMAPStore::new(config)),
        MailStoreConfig::MailDir(config) => Box::new(MailDirStore::new(config)),
        MailStoreConfig::IndexedMailDir(config) => {
            Box::new(IndexedMailDirStore::new(config, event_sink)?)
        }
    })
}
