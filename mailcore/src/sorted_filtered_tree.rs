use crate::sync_tree::{TreeChange, TreeListener};
use crate::tree::Tree;
use std::cmp::Ordering;
use std::collections::HashMap;
use std::ops::Index;
use std::sync::Arc;

pub struct SortedFilteredSyncedTree<T, L: TreeListener> {
    source: Arc<Tree<T>>,
    model: L,
    filter: Option<Box<dyn FnMut(&T) -> bool>>,
    sorter: Box<dyn FnMut(usize, &T, &T) -> Ordering>,
    tree: Tree<usize>,
    source_to_destination: HashMap<usize, usize>,
}

impl<T: Clone + PartialEq, L: TreeListener> SortedFilteredSyncedTree<T, L> {
    pub fn new(model: L) -> SortedFilteredSyncedTree<T, L>
    where
        T: Ord,
    {
        SortedFilteredSyncedTree {
            source: Arc::default(),
            model,
            filter: None,
            sorter: Box::new(|_, a, b| a.cmp(&b)),
            tree: Tree::default(),
            source_to_destination: HashMap::default(),
        }
    }
    #[allow(dead_code)]
    pub fn len(&self) -> usize {
        self.tree.len()
    }
    pub fn is_empty(&self) -> bool {
        self.tree.len() == 0
    }
    pub fn set_filter(&mut self, filter: Box<dyn FnMut(&T) -> bool>) {
        self.model.begin_reset_model();
        self.filter = Some(filter);
        self.filter_and_sort_tree();
        self.model.end_reset_model();
    }
    pub fn set_sorter(&mut self, sorter: Box<dyn FnMut(usize, &T, &T) -> Ordering>) {
        self.model.layout_about_to_be_changed();
        self.sorter = sorter;
        self.filter_and_sort_tree();
        self.model.layout_changed();
    }
    fn filter_and_sort_node(
        &mut self,
        parent: Option<usize>,
        dest_parent: Option<usize>,
        depth: usize,
    ) {
        let row_count = self.source.row_count(parent);
        let mut children = Vec::new();
        for row in 0..row_count {
            let index = self.source.index(parent, row);
            let mut filter = true;
            if let Some(f) = &mut self.filter {
                use std::ops::DerefMut;
                filter = show_node(index, depth, &self.source, f.deref_mut());
            }
            if filter {
                children.push(index);
            }
        }
        children.sort_by(|a, b| (*self.sorter)(depth, &self.source[*a], &self.source[*b]));
        for index in children {
            let dest_index = self.tree.append(dest_parent, index);
            self.source_to_destination.insert(index, dest_index);
            self.filter_and_sort_node(Some(index), Some(dest_index), depth + 1);
        }
    }
    fn filter_and_sort_tree(&mut self) {
        self.source_to_destination.clear();
        self.tree.clear();
        self.filter_and_sort_node(None, None, 0);
    }
    fn find_insert_position(&mut self, dest_parent: Option<usize>, source_index: usize) -> usize {
        let depth = self.source.depth(source_index);
        let source = &self.source;
        let item = &source[source_index];
        let sorter = &mut self.sorter;
        match self
            .tree
            .binary_search_by(dest_parent, |other_source_index| {
                let other = &source[*other_source_index];
                (*sorter)(depth, item, other)
            }) {
            Ok(pos) => pos,
            Err(pos) => pos,
        }
    }
    pub fn apply_all(&mut self, changes: Vec<TreeChange<T>>) {
        let mut needs_relayout = false;
        for change in &changes {
            if let TreeChange::Insert(_, _, _) = change {
                needs_relayout = true;
            }
        }
        for change in changes {
            self.apply(change);
        }
        if needs_relayout {
            self.model.layout_about_to_be_changed();
            self.filter_and_sort_tree();
            self.model.layout_changed();
        }
    }
    fn apply(&mut self, change: TreeChange<T>) {
        match change {
            TreeChange::Reset(tree) => {
                // avoid unneeded model reset
                if self.source != tree {
                    self.model.begin_reset_model();
                    self.source = tree;
                    self.filter_and_sort_tree();
                    self.model.end_reset_model();
                }
            }
            TreeChange::Insert(parent, pos, t) => {
                let index = Arc::make_mut(&mut self.source).insert(parent, pos, t);
                let dest_parent = parent
                    .as_ref()
                    .map(|parent| self.source_to_destination[parent]);
                let pos = self.find_insert_position(dest_parent, index);
                self.model.begin_insert_rows(parent, pos, pos);
                let dest_index = self.tree.insert(dest_parent, pos, index);
                self.source_to_destination.insert(index, dest_index);
                self.model.end_insert_rows();
            }
        }
    }
    pub fn row_count(&self, index: Option<usize>) -> usize {
        if let Some(index) = index {
            if let Some(index) = self.source_to_destination.get(&index) {
                self.tree.row_count(Some(*index))
            } else {
                0
            }
        } else {
            self.tree.row_count(None)
        }
    }
    pub fn index(&self, index: Option<usize>, row: usize) -> usize {
        let index = index
            .as_ref()
            .map(|index| self.source_to_destination[index]);
        let dest_index = self.tree.index(index, row);
        self.tree[dest_index]
    }
    pub fn parent(&self, index: usize) -> Option<usize> {
        self.source.parent(index)
    }
    // which row does index have in its parent
    pub fn row(&self, index: usize) -> usize {
        // get the destination node
        let index = self.source_to_destination[&index];
        self.tree.row(index)
    }
    pub fn check_row(&self, index: usize, row: usize) -> Option<usize> {
        self.tree.check_row(index, row)
    }
}

fn show_node<T>(
    index: usize,
    depth: usize,
    source: &Tree<T>,
    f: &mut dyn FnMut(&T) -> bool,
) -> bool {
    if f(&source[index]) {
        true
    } else {
        let parent = Some(index);
        let row_count = source.row_count(Some(index));
        let depth = depth + 1;
        (0..row_count).any(|row| {
            let index = source.index(parent, row);
            show_node(index, depth, source, f)
        })
    }
}

impl<T, L: TreeListener> Index<usize> for SortedFilteredSyncedTree<T, L> {
    type Output = T;
    fn index(&self, index: usize) -> &T {
        &self.source[index]
    }
}
