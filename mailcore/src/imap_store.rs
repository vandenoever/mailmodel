use crate::errors::Result;
use crate::mail_flags::MailFlags;
use crate::mail_store::{Folder, MailStore, Name};
use crate::mail_store_state::MailStoreState;
use crate::message::{Message, Uid};
use crate::message_threads::MessageThreader;
use imap::types::Mailbox;
use imap::{Client, Session};
use imap_proto::parser::parse_response;
use imap_proto::types::{AttributeValue, MessageSection, Response, SectionPath};
use native_tls::{TlsConnector, TlsStream};
use serde_derive::{Deserialize, Serialize};
use std::fmt;
use std::io::{Read, Write};
use std::net::TcpStream;
use std::sync::Arc;

pub struct ConnectionOnly(pub Session<Box<dyn Stream>>);

impl ConnectionOnly {
    pub fn examine(mut self, folder: &str) -> Result<ConnectionAndFolder> {
        let mailbox = self.0.examine(folder)?;
        Ok(ConnectionAndFolder {
            client: self.0,
            folder: folder.to_string(),
            mailbox,
        })
    }
}

pub struct ConnectionAndFolder {
    pub client: Session<Box<dyn Stream>>,
    pub folder: String,
    pub mailbox: Mailbox,
}

impl ConnectionAndFolder {
    pub fn examine(mut self, folder: &str) -> Result<ConnectionAndFolder> {
        if self.folder != folder {
            self.mailbox = self.client.examine(folder)?;
            self.folder = folder.to_string();
        }
        Ok(self)
    }
}

impl std::fmt::Display for Uid {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.0.fmt(f)
    }
}

#[derive(Deserialize, Debug, Serialize)]
pub struct IMAPConfig {
    domain: String,
    username: String,
    password: String,
    port: u16,
    ssl: bool,
}

pub enum Connection {
    None,
    ConnectionOnly(ConnectionOnly),
    ConnectionAndFolder(ConnectionAndFolder),
}

pub struct IMAPStore {
    config: IMAPConfig,
    connection: Connection,
}

impl IMAPStore {
    pub fn new(config: IMAPConfig) -> IMAPStore {
        IMAPStore {
            config,
            connection: Connection::None,
        }
    }
    fn only_connection(&mut self) -> Result<ConnectionOnly> {
        let mut c = Connection::None;
        std::mem::swap(&mut c, &mut self.connection);
        Ok(ConnectionOnly(match c {
            Connection::None => connect(&self.config)?,
            Connection::ConnectionOnly(c) => c.0,
            Connection::ConnectionAndFolder(c) => c.client,
        }))
    }
    fn examine(&mut self, folder: &str) -> Result<ConnectionAndFolder> {
        let mut c = Connection::None;
        std::mem::swap(&mut c, &mut self.connection);
        Ok(match c {
            Connection::ConnectionAndFolder(c) => c.examine(folder)?,
            _ => self.only_connection()?.examine(folder)?,
        })
    }
}

impl MailStore for IMAPStore {
    fn is_folder_complete(&self, folder: &str, state: &MailStoreState) -> bool {
        if let Some(f) = state.get_folder(folder) {
            let len = f.threads.len();
            if let Connection::ConnectionAndFolder(c) = &self.connection {
                return c.mailbox.exists as usize == len;
            }
        }
        false
    }
    fn list_folders(&mut self) -> Result<Vec<Folder>> {
        let (folders, connection) = list_imap_folders(self.only_connection()?)?;
        self.connection = Connection::ConnectionOnly(connection);
        Ok(folders)
    }
    fn fetch_headers(&mut self, folder_path: &str, threads: &mut MessageThreader) -> Result<()> {
        let start = threads.len();
        let step = 40;
        let mut connection = self.examine(folder_path)?;
        let amount = step.min((connection.mailbox.exists as usize).saturating_sub(start));
        connection = fetch_headers(connection, start, amount, threads)?;
        self.connection = Connection::ConnectionAndFolder(connection);
        Ok(())
    }
    fn fetch(&mut self, folder: &str, uid: Uid) -> Result<Message> {
        let connection = self.examine(folder)?;
        let (message, connection) = fetch(connection, uid)?;
        self.connection = Connection::ConnectionAndFolder(connection);
        Ok(message)
    }
}

pub trait Stream: Read + Write + Send {}
impl Stream for TcpStream {}
impl Stream for TlsStream<TcpStream> {}

pub fn connect_tcp(domain: &str, port: u16) -> Result<Box<dyn Stream>> {
    let socket_addr = (domain, port);
    Ok(Box::new(TcpStream::connect(socket_addr)?))
}

pub fn connect_tls(domain: &str, port: u16) -> Result<Box<dyn Stream>> {
    let socket_addr = (domain, port);
    let connector = TlsConnector::builder().build()?;
    let stream = TcpStream::connect(socket_addr)?;
    Ok(Box::new(connector.connect(domain, stream)?))
}

pub fn connect(config: &IMAPConfig) -> Result<Session<Box<dyn Stream>>> {
    let stream = if config.ssl {
        connect_tls(&config.domain, config.port)?
    } else {
        connect_tcp(&config.domain, config.port)?
    };
    let imap_socket = Client::new(stream);
    let session = imap_socket
        .login(&config.username, &config.password)
        .map_err(|e| e.0)?;
    Ok(session)
}

pub fn list_imap_folders(mut connection: ConnectionOnly) -> Result<(Vec<Folder>, ConnectionOnly)> {
    let mut folders = Vec::new();

    match connection.0.list(Some(""), Some("*")) {
        Ok(f) => {
            for f in &f {
                folders.push(Folder {
                    name: Name {
                        delimiter: f.delimiter().unwrap_or("").into(),
                        name: f.name().into(),
                    },
                    messages: None,
                    unread: None,
                });
            }
        }
        Err(e) => println!("Error listing: {}", e),
    }
    folders.sort_by(|a, b| a.name.name.cmp(&b.name.name));
    Ok((folders, connection))
}

fn parse_message(atts: &[AttributeValue]) -> Result<Message> {
    if atts.len() != 2 {
        return Err(format!("Atts length is not 2 but {}", atts.len()).into());
    }
    let uid = if let AttributeValue::Uid(uid) = atts[0] {
        uid as usize
    } else {
        return Err(format!("First item in atts is not a Uid but {:?}.", atts[0]).into());
    };
    let header_data = if let AttributeValue::BodySection {
        section: Some(SectionPath::Full(MessageSection::Header)),
        index: None,
        data: Some(data),
    } = &atts[1]
    {
        data
    } else {
        return Err(format!("Second item in atts is not a headers but {:?}.", atts[1]).into());
    };
    Ok(Message::from_header_bytes(
        Uid(uid),
        Some(MailFlags::from_seen(true)),
        header_data,
        None,
    )?)
}

pub fn fetch_headers(
    mut connection: ConnectionAndFolder,
    start: usize,
    amount: usize,
    threads: &mut MessageThreader,
) -> Result<ConnectionAndFolder> {
    // alternative:
    // FETCH 1:* BODY.PEEK[HEADER.FIELDS (DATE FROM SUBJECT MESSAGE-ID REFERENCES)]
    let response_bytes = connection.client.run_command_and_read_response(&format!(
        "FETCH {}:{} (UID BODY.PEEK[HEADER])",
        start + 1,
        start + amount
    ))?;
    let mut remainder: &[u8] = &response_bytes;
    loop {
        match parse_response(remainder) {
            Ok((r, Response::Fetch(_, atts))) => {
                remainder = r;
                let message = Arc::new(parse_message(&atts)?);
                threads.add_message(message);
            }
            Ok((_, r)) => {
                return Err(format!("Unexpected result for parse_response {:?}", r).into());
            }
            Err(nom::Err::Incomplete(needed)) => {
                return Err(format!(
                    "fetch_header result is incomplete, needed {:?} more bytes to parse '{}'",
                    needed,
                    String::from_utf8_lossy(&response_bytes)
                )
                .into());
            }
            Err(e) => return Err(e.into()),
        };
        if remainder.is_empty() {
            return Ok(connection);
        }
    }
}

pub fn fetch(
    mut connection: ConnectionAndFolder,
    uid: Uid,
) -> Result<(Message, ConnectionAndFolder)> {
    let response_bytes = connection
        .client
        .run_command_and_read_response(&format!("UID FETCH {} (UID BODY.PEEK[])", uid))?;
    match parse_response(&response_bytes) {
        Ok((_, Response::Fetch(_, atts))) => {
            if let AttributeValue::BodySection {
                data: Some(data), ..
            } = &atts[1]
            {
                return Ok((
                    Message::from_bytes(
                        uid,
                        Some(MailFlags::from_seen(true)),
                        data.to_vec(),
                        None,
                    )?,
                    connection,
                ));
            }
            Err(format!("Unexpected result for parse_response {:?}", atts).into())
        }
        Ok((_, r)) => Err(format!("Unexpected result for parse_response {:?}", r).into()),
        Err(nom::Err::Incomplete(needed)) => Err(format!(
            "fetch result is incomplete, needed {:?} more bytes to parse '{}'",
            needed,
            String::from_utf8_lossy(&response_bytes)
        )
        .into()),
        Err(e) => Err(e.into()),
    }
}
