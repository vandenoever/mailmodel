use rusqlite::Connection;
use std::path::Path;
use std::thread;
use std::time::Duration;

pub fn open<P: AsRef<Path>>(path: P) -> rusqlite::Result<Connection> {
    let connection = Connection::open(path)?;
    fn handle(_: i32) -> bool {
        thread::sleep(Duration::from_micros(100));
        true
    }
    connection.busy_handler(Some(handle)).unwrap();
    connection.execute_batch(
        "PRAGMA journal_mode = WAL;
         PRAGMA synchronous = NORMAL;
         PRAGMA temp_store = MEMORY;
         CREATE TABLE IF NOT EXISTS blob (
             id              INTEGER PRIMARY KEY,
             length          INTEGER NOT NULL,
             sha256          BLOB NOT NULL
         );
         CREATE UNIQUE INDEX IF NOT EXISTS blob_sha256 ON blob(sha256);
         CREATE TABLE IF NOT EXISTS mail_folder (
             id              INTEGER PRIMARY KEY,
             path            TEXT NOT NULL,
             name            TEXT NOT NULL
         );
         CREATE UNIQUE INDEX IF NOT EXISTS mail_folder_path ON mail_folder(path);
         CREATE UNIQUE INDEX IF NOT EXISTS mail_folder_name ON mail_folder(name);
         CREATE TABLE IF NOT EXISTS mail_file (
             id              INTEGER PRIMARY KEY,
             folder          INTEGER NOT NULL,
             new             BOOL NOT NULL,
             seen            BOOL NOT NULL,
             file_name       TEXT NOT NULL,
             mtime           INTEGER NOT NULL,
             blob            INTEGER,
             FOREIGN KEY(folder) REFERENCES mail_folder(id) ON DELETE CASCADE,
             FOREIGN KEY(blob) REFERENCES blob(id)
         );
         CREATE UNIQUE INDEX IF NOT EXISTS mail_file_path ON mail_file(folder, new, file_name);
         CREATE INDEX IF NOT EXISTS mail_file_blob ON mail_file(blob);
         CREATE TABLE IF NOT EXISTS mail (
             id              INTEGER PRIMARY KEY,
             subject         TEXT NOT NULL,
             from_           TEXT NOT NULL,
             message_id      TEXT NOT NULL,
             in_reply_to     TEXT,
             date            INTEGER NOT NULL,
             FOREIGN KEY(id) REFERENCES blob(id)
         );
         CREATE INDEX IF NOT EXISTS mail_subject ON mail(subject);
         CREATE INDEX IF NOT EXISTS mail_from ON mail(from_);
         CREATE INDEX IF NOT EXISTS mail_message_id ON mail(message_id);
         CREATE INDEX IF NOT EXISTS mail_in_reply_to ON mail(in_reply_to);
         CREATE INDEX IF NOT EXISTS mail_date ON mail(date);",
    )?;
    //profile(&mut connection);
    Ok(connection)
}

#[allow(dead_code)]
fn profile(db: &mut Connection) {
    db.profile(Some(|sql, duration| {
        let secs = duration.as_secs() as f32 + duration.subsec_millis() as f32 / 1000.;
        if secs > 0. {
            println!("{} {}", secs, sql);
        }
    }));
}
