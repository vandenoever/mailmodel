use crate::errors::Result;
use crate::gpg;
use crate::mail_store::{
    create_mail_store, EventHandler, Folder, MailStore, MailStoreChange, Name,
};
use crate::mail_store_state::{MailFolder, MailStoreState};
use crate::message::{Message, Uid};
use crate::message_threads::MessageThreader;
use crate::sync_tree::TreeChange;
use std::collections::HashMap;
use std::ops::DerefMut;
use std::path::Path;
use std::sync::mpsc::{channel, Receiver, SendError, Sender};
use std::sync::{Arc, Mutex};
use std::thread;

pub trait Observer: Send + 'static {
    fn done(&self) -> bool;
}

pub trait FoldersReceiver: Observer {
    fn set_folders(&self, _: Vec<Folder>);
}

pub trait FolderReceiver: Observer {
    fn add_changes(&mut self, changes: Vec<TreeChange<Arc<Message>>>);
}

pub trait EmailReceiver: Send + 'static {
    fn set_email(&self, _: Option<Arc<Message>>);
}

pub enum Job<R: FoldersReceiver, F: FolderReceiver, E: EmailReceiver> {
    GetFolders(R),
    SetFolder(F, String),
    SetEmail(E, String, Uid),
    Stop,
}

struct JobThread<R: FoldersReceiver, F: FolderReceiver, E: EmailReceiver> {
    job_rx: Receiver<Job<R, F, E>>,
    shared: Arc<Mutex<Shared>>,
    handler: Arc<Mutex<Handler>>,
}

struct Shared {
    mail_store: Option<Box<dyn MailStore>>,
    current_folder: Option<String>,
    last_folder: Option<String>,
    state: MailStoreState,
}

impl Shared {
    fn mail_store(&mut self) -> Result<&mut dyn MailStore> {
        if let Some(mail_store) = &mut self.mail_store {
            Ok(mail_store.deref_mut())
        } else {
            bail!("No mail store was initiated.")
        }
    }
    fn is_folder_complete(&self) -> bool {
        if let Some(folder) = &self.current_folder {
            if let Some(mail_store) = &self.mail_store {
                mail_store.is_folder_complete(folder, &self.state)
            } else {
                false
            }
        } else {
            true
        }
    }
    fn set_config<P: AsRef<Path>>(
        &mut self,
        config_path: P,
        handler: Arc<Mutex<Handler>>,
    ) -> Result<()> {
        self.state = MailStoreState::default();
        match create_mail_store(config_path, handler) {
            Ok(mail_store) => {
                self.mail_store = Some(mail_store);
                Ok(())
            }
            Err(err) => {
                self.mail_store = None;
                Err(err)
            }
        }
    }
    fn list_folders<R: FoldersReceiver>(&mut self, receiver: &R) -> Result<()> {
        let folders = self.mail_store()?.list_folders()?;
        receiver.set_folders(folders);
        Ok(())
    }
    fn get_folder(&mut self, folder: &mut dyn FolderReceiver, folder_name: &str) {
        let changes = self.get_changes(folder_name);
        folder.add_changes(changes);
    }
    fn get_changes(&mut self, folder_name: &str) -> Vec<TreeChange<Arc<Message>>> {
        self.current_folder = Some(folder_name.to_string());
        let mut changes = Vec::new();
        self.fetch_from_folder(folder_name);
        let f = self.state.get_folder(&folder_name);
        if let Some(f) = &f {
            changes.push(TreeChange::Reset(f.threads.tree().clone()));
        } else {
            changes.push(TreeChange::Reset(Arc::default()));
        }
        changes
    }
    fn fetch_more_folder(&mut self) -> bool {
        if self.is_folder_complete() {
            return false;
        }
        // fetch from the most recently gotten folder
        if let Some(folder_name) = self.current_folder.clone() {
            self.fetch_from_folder(&folder_name);
            true
        } else {
            false
        }
    }
    fn fetch_from_folder(&mut self, folder_name: &str) {
        let folder = self.state.get_folder(folder_name);
        match self.list_folder(folder, folder_name) {
            Err(e) => eprintln!("Error for command: {}", e),
            Ok(folder) => {
                self.state
                    .set_folder(folder_name.to_string(), Arc::clone(&folder));
            }
        }
    }
    fn list_folder(
        &mut self,
        folder: Option<Arc<MailFolder>>,
        folder_path: &str,
    ) -> Result<Arc<MailFolder>> {
        let threads = if let Some(folder) = folder {
            folder.threads.clone()
        } else {
            Arc::default()
        };
        let new_folder = Some(folder_path) == self.last_folder.as_deref();

        let mut threader = MessageThreader::new(&threads, threads.tree().clone(), !new_folder);
        self.mail_store()?
            .fetch_headers(folder_path, &mut threader)?;
        let (_changes, threads) = threader.done();
        Ok(Arc::new(MailFolder::new(Arc::new(threads))))
    }
}

impl<R: FoldersReceiver, F: FolderReceiver, E: EmailReceiver> JobThread<R, F, E> {
    fn start(
        handler: Arc<Mutex<Handler>>,
        job_receiver: Receiver<Job<R, F, E>>,
    ) -> thread::JoinHandle<()> {
        let shared = handler.lock().unwrap().shared.clone();
        thread::spawn(move || {
            let mut job_thread = JobThread {
                job_rx: job_receiver,
                shared,
                handler,
            };
            let mut r = job_thread.job_rx.recv().ok();
            loop {
                r = job_thread.thread_loop(r);
                if r.is_none() {
                    return;
                }
            }
        })
    }
    fn thread_loop(&mut self, r: Option<Job<R, F, E>>) -> Option<Job<R, F, E>> {
        let r = match r {
            None | Some(Job::Stop) => {
                // to avoid a lock, erase the shared in self and handler
                let shared = self.shared.clone();
                self.handler.lock().unwrap().shared = shared_new();
                self.shared = shared_new();
                // destroy the mail_store object
                shared.lock().unwrap().mail_store = None;
                eprintln!("stopping mail model thread");
                return None;
            }
            Some(Job::GetFolders(folders)) => {
                let _ = self.shared.lock().unwrap().list_folders(&folders);
                self.handler
                    .lock()
                    .unwrap()
                    .folders_receivers
                    .push(Box::new(folders));
                Ok(())
            }
            Some(Job::SetFolder(mut folder, folder_name)) => {
                self.shared
                    .lock()
                    .unwrap()
                    .get_folder(&mut folder, &folder_name);
                self.handler
                    .lock()
                    .unwrap()
                    .folder_receivers
                    .entry(folder_name)
                    .or_default()
                    .push(Box::new(folder));
                Ok(())
            }
            Some(Job::SetEmail(email, folder, uid)) => self.set_email(&email, &folder, uid),
        };
        if let Err(e) = r {
            eprintln!("Error for command: {}", e);
            for e in e.iter().skip(1) {
                eprintln!("caused by: {}", e);
            }
        }
        // while the folder is not completely read and there is no other job
        // read this folder
        loop {
            if let Ok(job) = self.job_rx.try_recv() {
                return Some(job);
            }
            if !self.shared.lock().unwrap().fetch_more_folder() {
                break;
            }
        }
        self.job_rx.recv().ok()
    }
    fn set_email(&mut self, email: &E, folder: &str, uid: Uid) -> Result<()> {
        let message = self
            .shared
            .lock()
            .unwrap()
            .mail_store()?
            .fetch(folder, uid)?;
        let msg = gpg::decrypt(message).0;
        email.set_email(Some(Arc::new(msg)));
        Ok(())
    }
}

pub struct JobProcessor<R: FoldersReceiver, F: FolderReceiver, E: EmailReceiver> {
    job_tx: Sender<Job<R, F, E>>,
    handle: Option<thread::JoinHandle<()>>,
}

fn shared_new() -> Arc<Mutex<Shared>> {
    Arc::new(Mutex::new(Shared {
        mail_store: None,
        current_folder: None,
        last_folder: None,
        state: MailStoreState::default(),
    }))
}

impl<R: FoldersReceiver, F: FolderReceiver, E: EmailReceiver> JobProcessor<R, F, E> {
    pub fn create<P: AsRef<Path>>(config_path: P) -> Result<JobProcessor<R, F, E>> {
        let shared = shared_new();
        let handler = Arc::new(Mutex::new(Handler::new(shared.clone())));
        shared
            .lock()
            .unwrap()
            .set_config(config_path, handler.clone())?;
        let (job_tx, job_rx) = channel();
        let handle = JobThread::start(handler, job_rx);
        Ok(JobProcessor {
            job_tx,
            handle: Some(handle),
        })
    }
    pub fn send(&self, job: Job<R, F, E>) -> std::result::Result<(), SendError<Job<R, F, E>>> {
        self.job_tx.send(job)
    }
}

impl<R: FoldersReceiver, F: FolderReceiver, E: EmailReceiver> Default for JobProcessor<R, F, E> {
    fn default() -> JobProcessor<R, F, E> {
        let shared = Arc::new(Mutex::new(Shared {
            mail_store: None,
            current_folder: None,
            last_folder: None,
            state: MailStoreState::default(),
        }));
        let handler = Arc::new(Mutex::new(Handler::new(shared)));
        let (job_tx, job_rx) = channel();
        let handle = JobThread::start(handler, job_rx);
        JobProcessor {
            job_tx,
            handle: Some(handle),
        }
    }
}

impl<R: FoldersReceiver, F: FolderReceiver, E: EmailReceiver> Drop for JobProcessor<R, F, E> {
    fn drop(&mut self) {
        let _ = self.job_tx.send(Job::Stop);
        self.job_tx = channel().0; // close the channel by dropping it
        self.handle.take().map(|handle| handle.join());
    }
}

struct Handler {
    folders_receivers: Vec<Box<dyn FoldersReceiver>>,
    folder_receivers: HashMap<String, Vec<Box<dyn FolderReceiver>>>,
    shared: Arc<Mutex<Shared>>,
}

impl Handler {
    fn new(shared: Arc<Mutex<Shared>>) -> Handler {
        Handler {
            folders_receivers: Vec::new(),
            folder_receivers: HashMap::new(),
            shared,
        }
    }
    fn handle_folders_list_changed(&mut self) {
        self.folders_receivers.retain(|f| !f.done());
        // erase the folders cache
        self.shared.lock().unwrap().state.clear();
        // ask for a new list of folders
        for f in &self.folders_receivers {
            if let Err(e) = self.shared.lock().unwrap().list_folders(f) {
                eprintln!("{:?}", e);
            }
        }
    }
    fn handle_folder_changed(&mut self, folder_name: Name) {
        self.shared.lock().unwrap().state.clear();
        // look up any receivers for this folder
        if let Some(mut folder_receivers) = self.folder_receivers.remove(&folder_name.name) {
            folder_receivers.retain(|f| !f.done());
            if !folder_receivers.is_empty() {
                let changes = self.shared.lock().unwrap().get_changes(&folder_name.name);
                for f in &mut folder_receivers {
                    f.add_changes(changes.clone());
                }
                self.folder_receivers
                    .insert(folder_name.name, folder_receivers);
            }
        }
    }
}

impl EventHandler for Handler {
    fn handle_event(&mut self, event: MailStoreChange) {
        match event {
            MailStoreChange::FoldersListChanged => {
                self.handle_folders_list_changed();
            }
            MailStoreChange::FolderChanged(folder_name) => {
                self.handle_folder_changed(folder_name);
            }
        }
    }
}
