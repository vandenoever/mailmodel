use lettre::{SendableEmail, SmtpClient, SmtpTransport, Transport};
use lettre_email::EmailBuilder;
use std::io::Read;
use std::path::{Path, PathBuf};

struct FileEmailTransport {
    directory: PathBuf,
}

impl FileEmailTransport {
    pub fn new<P: AsRef<Path>>(directory: P) -> FileEmailTransport {
        FileEmailTransport {
            directory: directory.as_ref().to_path_buf(),
        }
    }
}

impl<'a> Transport<'a> for FileEmailTransport {
    type Result = Result<(), std::io::Error>;
    fn send(&mut self, email: SendableEmail) -> Self::Result {
        let path = self.directory.join(email.message_id());
        let mut msg = email.message();
        let mut buf = Vec::new();
        msg.read_to_end(&mut buf)?;
        std::fs::write(path, &buf)?;
        Ok(())
    }
}

fn main() {
    let builder = EmailBuilder::new()
        .from("zbigniew@siciarz.net")
        .to("zbigniew@siciarz.net")
        .subject("Hello Rust!")
        .body("Hello Rust!")
        .attachment_from_file(&PathBuf::from("Cargo.toml"), None, &mime::TEXT_PLAIN)
        .unwrap();
    let email = builder.build().expect("Failed to build message");
    let mut transport = FileEmailTransport::new(".");
    println!("{:?}", transport.send(email.clone().into()));
    //println!("'{}'", email.text());
    let client = SmtpClient::new_unencrypted_localhost().unwrap();
    let mut transport = SmtpTransport::new(client);
    println!("{:?}", transport.send(email.into()));
}
