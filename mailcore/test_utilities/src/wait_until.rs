use std::sync::{Arc, Condvar, Mutex};
use std::time::{Duration, SystemTime};

pub fn wait_until<D>(data: &Arc<(Mutex<D>, Condvar)>, f1: fn(&D) -> bool, check: fn(&D)) {
    let &(ref lock, ref cvar) = &**data;
    let mut data = lock.lock().unwrap();
    let now = SystemTime::now();
    let mut remaining_wait = Duration::from_millis(5000);
    while !f1(&data) {
        let r = cvar.wait_timeout(data, remaining_wait).unwrap();
        if r.1.timed_out() {
            panic!("Wait timed out");
        }
        remaining_wait -= now.elapsed().unwrap();
        data = r.0;
    }
    check(&data);
}
