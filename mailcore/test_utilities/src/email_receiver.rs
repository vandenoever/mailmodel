use crate::wait_until::wait_until;
use mailcore::message::Message;
use mailcore::sync_tree::TreeListener;
use mailcore::Observer;
use std::sync::{Arc, Condvar, Mutex};

#[derive(Default)]
pub struct Null {}

impl TreeListener for Null {}

pub struct EmailReceiverData {
    pub done: bool,
    pub mail: Option<Arc<Message>>,
    pub updates: usize,
}

#[derive(Clone)]
pub struct EmailReceiver {
    data: Arc<(Mutex<EmailReceiverData>, Condvar)>,
}

impl EmailReceiver {
    pub fn set_done(&mut self, done: bool) {
        self.data.0.lock().unwrap().done = done;
    }
    pub fn wait_until(&self, f1: fn(&EmailReceiverData) -> bool, check: fn(&EmailReceiverData)) {
        wait_until(&self.data, f1, check);
    }
}

impl Default for EmailReceiver {
    fn default() -> EmailReceiver {
        EmailReceiver {
            data: Arc::new((
                Mutex::new(EmailReceiverData {
                    done: false,
                    mail: None,
                    updates: 0,
                }),
                Condvar::new(),
            )),
        }
    }
}

impl Observer for EmailReceiver {
    fn done(&self) -> bool {
        self.data.0.lock().unwrap().done
    }
}

impl mailcore::EmailReceiver for EmailReceiver {
    fn set_email(&self, mail: Option<Arc<Message>>) {
        let &(ref lock, ref cvar) = &*self.data;
        let mut data = lock.lock().unwrap();
        data.mail = mail;
        data.updates += 1;
        cvar.notify_one();
    }
}
