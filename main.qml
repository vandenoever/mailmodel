import QtQuick 2.7
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.2
import QtQuick.Window 2.2
import RustCode 1.0

Window {
    id: mainwindow
    visible: true
    width: 1024
    height: 800
    title: qsTr("Mail Viewer")

    MailModel {
        id: mailmodel
        configFile: mailConfigFile
    }

    SystemPalette {
        id: palette
        colorGroup: SystemPalette.Active
    }

    FontMetrics {
        id: systemFont
    }

    ColumnLayout {
        anchors.fill: parent
        SplitView {
            Layout.fillHeight: true
            Layout.fillWidth: true
            handleDelegate: Rectangle {
                width: 3
                color: palette.window
            }
            FolderTree {
                id: folders
                model: mailmodel.folders
                onCurrentFolderChanged: mailmodel.currentFolder = currentFolder
            }
            ThreadsView {
                id: threads
                onCurrentUidChanged: if (currentUid !== undefined) {
                    mailmodel.setEmail(currentUid)
                }
                onFilterChanged: mailmodel.folder_threads.setFilter(filter)
            }
            Email {}
            Component.onCompleted: threads.width = threads.Layout.preferredWidth
        }
        Rectangle {
            visible: label.text
            color: "yellow"
            width: childrenRect.width
            height: childrenRect.height
            Label {
                id: label
                text: mailmodel.folder_threads.error || ""
            }
        }
    }
    function expand(treeview, index) {
        treeview.expand(index);
        var rowCount = treeview.model.rowCount(index);
        var row;
        for (row = 0; row < rowCount; ++row) {
            expand(treeview, treeview.model.index(row, 0, index));
        }
    }
    function expandRecursive(treeview, index) {
        treeview.expand(index);
        var rowCount = treeview.model.rowCount(index);
        var row;
        for (row = 0; row < rowCount; ++row) {
            expandRecursive(treeview, treeview.model.index(row, 0, index));
        }
    }
    function expandAll(treeview) {
        expandRecursive(treeview, treeview.rootIndex);
    }
    Component.onCompleted: {
        folders.model.modelReset.connect(function () {
            expandAll(folders);
        });
    }
    function icon(name) {
        if (name.toLowerCase() === "inbox") {
            return "📥";
        }
        if (name.toLowerCase() === "trash") {
            return "🗑";
        }
        if (name.toLowerCase() === "outbox") {
            return "📤";
        }
        return "📂";
    }
    Action {
        text: qsTr('Select folder')
        shortcut: 'j'
        enabled: true
        onTriggered: mailFolderSelectDialog.visible = true
    }
    Action {
        text: qsTr('New Message')
        shortcut: StandardKey.New
        enabled: true
        onTriggered: createComposer()
    }
    Action {
        text: qsTr('Reply')
        shortcut: 'r'
        enabled: mailmodel.email.date // is non-empty when a mail is selected
        onTriggered: replyMail()
    }
    Action {
        text: qsTr('Reply all')
        shortcut: 'a'
        enabled: mailmodel.email.date // is non-empty when a mail is selected
        onTriggered: replyAllMail()
    }
    MailFolderSelectDialog {
        id: mailFolderSelectDialog
        visible: false
        onAccepted: folders.setCurrentFolder(mailFolderSelectDialog.currentFolder);
    }
    function createComposer(fill) {
        let component = Qt.createComponent("EmailForm.qml");
        if (component.status !== Component.Ready) {
            console.log("Component not ready", component.errorString());
        } else {
            let composer = component.createObject(mainwindow);
            if (fill) {
                fill(composer);
            }
            composer.show();
        }
    }
    function replyMail() {
        createComposer(function (composer) {
            let bytes = mailmodel.email.bytes
            let email = mailmodel.email
            composer.initReplyFromMailBytes(bytes)
        })
    }
    function replyAllMail() {
        createComposer(function (composer) {
            let bytes = mailmodel.email.bytes
            let email = mailmodel.email
            composer.initReplyAllFromMailBytes(bytes)
        })
    }
}
