/*
 *   Copyright 2018  Jos van den Oever <jos@vandenoever.info>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License as
 *   published by the Free Software Foundation; either version 2 of
 *   the License or (at your option) version 3 or any later version
 *   accepted by the membership of KDE e.V. (or its successor approved
 *   by the membership of KDE e.V.), which shall act as a proxy
 *   defined in Section 14 of version 3 of the license.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Bindings.h"
#include "highlighter.h"

#include <QtCore/QFile>
#include <QtGui/QGuiApplication>
#include <QtQml/QQmlApplicationEngine>
#include <QtQml/QQmlContext>
#include <QtQml/qqml.h>

#include <QtNetwork/QNetworkAccessManager>
#include <QtQml/QQmlNetworkAccessManagerFactory>

class NoNetworkAccessManagerFactory : public QQmlNetworkAccessManagerFactory
{
public:
    QNetworkAccessManager *create(QObject * /*parent*/) override {
        auto n = new QNetworkAccessManager();
        n->setNetworkAccessible(QNetworkAccessManager::NotAccessible);
        return n;
    }
};

extern "C" {
    int main_cpp(const char* app, const char* rawConfigFile);
}

int main_cpp(const char* appPath, const char* rawConfigFile)
{
    QString configFile = QString::fromUtf8(rawConfigFile);
    int argc = 1;
    char* argv[1] = { (char*)appPath };
    QGuiApplication app(argc, argv);
    app.setOrganizationDomain("kde.org");
    app.setOrganizationName("KDE");
    qmlRegisterType<MailModel>("RustCode", 1, 0, "MailModel");
    qmlRegisterType<EmailForm>("RustCode", 1, 0, "EmailModel");
    qmlRegisterType<EmailSyntaxHighlighter>("RustCode", 1, 0, "EmailSyntaxHighlighter");

    NoNetworkAccessManagerFactory networkManagerFactory;
    QQmlApplicationEngine engine;
    engine.setNetworkAccessManagerFactory(&networkManagerFactory);
    QQmlContext* context = engine.rootContext();
    context->setContextProperty("mailConfigFile", configFile);
    if (QFile("main.qml").exists()) {
        engine.load(QUrl(QStringLiteral("file:main.qml")));
    } else {
        engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    }
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
