#ifndef HIGHLIGHTER_H
#define HIGHLIGHTER_H

#include <QtGui/QSyntaxHighlighter>
#include <QtCore/QRegularExpression>

class QQuickTextDocument;
class MailSyntaxHighlighter : public QSyntaxHighlighter {
public:
    MailSyntaxHighlighter();
protected:
    void highlightBlock(const QString &text);
private:
    const QRegularExpression pattern;
    const QTextCharFormat format;
};

class EmailSyntaxHighlighter : public QObject {
    Q_OBJECT
    Q_PROPERTY(QQuickTextDocument* document WRITE setDocument FINAL)
    MailSyntaxHighlighter m_highlighter;
public:
    void setDocument(QQuickTextDocument* document);
    EmailSyntaxHighlighter(QObject *parent = 0);
};

#endif
